# UmgToVRConverter

Two modules for UE4 which I developed for converting UMG widgets into real interactive VR controls. Users can press buttons like those in the real world. Modules are part of the bigger project which main focus was to allow users to meet together in the VR and present a 3D model to other connected players (in this case a house is presented on screens).

Process of converting UMG widgets into VR Controls is presented on the screens below. You can design widgets and implement logic in blueprints then smart algorithms are used to create corresponding VR controls.

Module VRControls was also used in other projects i.e. to make cockpit of an aircfat interactive.

![alt tag](./DemoScreens/1.PNG)
![alt tag](./DemoScreens/2.PNG)
![alt tag](./DemoScreens/3.PNG)
![alt tag](./DemoScreens/4.PNG)