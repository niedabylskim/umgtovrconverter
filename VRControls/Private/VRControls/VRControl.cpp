// Fill out your copyright notice in the Description page of Project Settings.

#include "VRControl.h"

#include <Components/StaticMeshComponent.h>
#include <UObject/ConstructorHelpers.h>

#include <VRUtilities/Gripper/VRGripper.h>

// Sets default values
AVRControl::AVRControl()
{
	//root = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	//RootComponent = root;
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VRControlRoot"));
	SetMeshFromPath("StaticMesh'/Engine/BasicShapes/Cube.Cube'");
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	bIsEnabled = true;

	InputCount = 0;
	OutputCount = 0;

	EnableMultiplayer();
}

void AVRControl::EnableMultiplayer() {
	bReplicates = true;
}

void AVRControl::InitBase(FTransform initialTransform, UStaticMesh* mesh, EControlInteractibleAxis interactionAxis, bool isEnabled)
{
	this->SetActorTransform(initialTransform);
	SetMesh(mesh, initialTransform.GetScale3D());
	bIsEnabled = isEnabled;
	SetInteractionAxis(interactionAxis);	
}

void AVRControl::InitBase(FTransform initialTransform, UProceduralMeshComponent* mesh, EControlInteractibleAxis interactionAxis, bool isEnabled)
{
	this->SetActorTransform(initialTransform);
	bIsEnabled = isEnabled;
	SetInteractionAxis(interactionAxis);
	SetMesh(mesh, initialTransform.GetScale3D());
}

void AVRControl::SetInteractionAxis(EControlInteractibleAxis newAxis)
{
	SetInteractionAxis(newAxis, InteractionAxis);
	interAxis = newAxis;
}

void AVRControl::SetRotationAxis(EControlInteractibleAxis newRotationAxis)
{
	SetInteractionAxis(newRotationAxis, RotationAxis);
	rotAxis = newRotationAxis;
}

void AVRControl::SetMesh(UStaticMesh* _mesh, FVector scale)
{
	if (mesh != nullptr && Cast<UProceduralMeshComponent>(mesh) != nullptr)
	{
		mesh->DestroyComponent();
		mesh = NewObject<UStaticMeshComponent>(this);
	}
	if (_mesh != nullptr)
	{
		auto staticMesh = Cast<UStaticMeshComponent>(mesh);
		staticMesh->SetStaticMesh(_mesh);

		staticMesh->SetWorldScale3D(scale);
		staticMesh->SetCollisionResponseToAllChannels(ECR_Overlap);
		RootComponent = mesh;
	}
}

void AVRControl::SetMesh(UProceduralMeshComponent * _mesh, FVector scale)
{
	if (mesh != nullptr && Cast<UStaticMeshComponent>(mesh) != nullptr)
	{
		mesh->DestroyComponent();
	}
	if (_mesh != nullptr)
	{
		mesh = _mesh;
		mesh->SetWorldScale3D(scale);
		mesh->SetCollisionResponseToAllChannels(ECR_Overlap);
		//mesh->AttachTo(RootComponent);
		RootComponent = mesh;
	}
}

void AVRControl::SetMeshFromPath(FString path, FVector scale)
{
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(path.GetCharArray().GetData());
	if (MeshAsset.Object != nullptr)
	{
		//if (!mesh) mesh = NewObject<UStaticMeshComponent>(this, UStaticMeshComponent::StaticClass(), TEXT("mesh"));
		auto staticMesh = Cast<UStaticMeshComponent>(mesh);
		staticMesh->SetStaticMesh(MeshAsset.Object);
		staticMesh->SetWorldScale3D(scale);
		staticMesh->SetCollisionResponseToAllChannels(ECR_Overlap);
		RootComponent = mesh;					  
	}
}


void AVRControl::SetInteractionAxis(EControlInteractibleAxis newAxis, FVector & vec)
{
	switch (newAxis)
	{
	case EControlInteractibleAxis::Axis_X:
		vec.X = 1.0f;
		vec.Y = 0.0f;
		vec.Z = 0.0f;
		break;
	case EControlInteractibleAxis::Axis_Y:
		vec.X = 0.0f;
		vec.Y = 1.0f;
		vec.Z = 0.0f;
		break;
	case EControlInteractibleAxis::Axis_Z:
		vec.X = 0.0f;
		vec.Y = 0.0f;
		vec.Z = 1.0f;
		break;
	case EControlInteractibleAxis::Axis_XY:
		vec.X = 1.0f;
		vec.Y = 1.0f;
		vec.Z = 1.0f;
		break;
	}
}

// Called when the game starts or when spawned
void AVRControl::BeginPlay()
{
	Super::BeginPlay();
	//check(mesh);
	SetInteractionAxis(interAxis);
	SetRotationAxis(rotAxis);
	ResetControlLocation();
	mesh->OnComponentBeginOverlap.AddDynamic(this, &AVRControl::OnOverlapBegin);
	mesh->OnComponentEndOverlap.AddDynamic(this, &AVRControl::OnOverlapEnd);
}

// Called every frame
void AVRControl::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AVRControl::ResetControlLocation()
{
	InitialRelativeTransform = mesh->GetRelativeTransform();
}

bool AVRControl::CheckIfGripShouldBreak(UVRGripper *VRGripper)
{
	return FVector::DistSquared(InitialDropLocation, mesh->GetComponentTransform().InverseTransformPosition(
		VRGripper->GetComponentLocation())) >= FMath::Square(BreakDistance);
}

void AVRControl::OnGripTick_Implementation(UVRGripper *VRGripper, float DeltaTime)
{
}

void AVRControl::OnGripRelease_Implementation(UVRGripper *VRGripper)
{
}

void AVRControl::OnGrip_Implementation(UVRGripper *VRGripper)
{
}

bool AVRControl::ShouldAttachToHand_Implementation() {
	return false;
}

void AVRControl::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void AVRControl::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

bool AVRControl::IsValidOverlap(UPrimitiveComponent * OverlapComponent)
{
	if (!OverlapComponent || OverlapComponent == mesh->GetAttachParent() || OverlapComponent->GetAttachParent() == mesh->GetAttachParent())
		return false;
	if (!OverlapComponent->ComponentHasTag(FName("VRGripperableColliderTag"))) return false;
	AActor * OverlapOwner = OverlapComponent->GetOwner();
	if (OverlapOwner && OverlapOwner->IsA(ACharacter::StaticClass()))
		return true;
	// Because epic motion controllers are not owned by characters have to check here too in case someone implements it like that
	// Now since our grip controllers are a subclass to the std ones we only need to check for the base one instead of both.
	//USceneComponent * OurAttachParent = OverlapComponent->GetAttachParent();
	//if (OurAttachParent && OurAttachParent->IsA(UMotionControllerComponent::StaticClass()))
	//	return true;
	//// Now check for if it is a grippable object and if it is currently held
	//if (OverlapComponent->GetClass()->ImplementsInterface(UVRGripInterface::StaticClass()))
	//{
	//	UGripMotionControllerComponent *Controller;
	//	bool bIsHeld;
	//	IVRGripInterface::Execute_IsHeld(OverlapComponent, Controller, bIsHeld);
	//	if (bIsHeld)
	//		return true;
	//}
	return true;
}