// Fill out your copyright notice in the Description page of Project Settings.

#include "VRControlSpawnTester.h"
#include "VRControl_Button.h"
#include <UObject/ConstructorHelpers.h>
// Sets default values
AVRControlSpawnTester::AVRControlSpawnTester()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FString path = "StaticMesh'/Game/VRControlsContent/ButtonSwitch/switch.switch_button-proste_osie'";
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(path.GetCharArray().GetData());
	if (MeshAsset.Object != nullptr)
	{
		mesh = MeshAsset.Object;
	}
}

// Called when the game starts or when spawned
void AVRControlSpawnTester::BeginPlay()
{
	Super::BeginPlay();

	UWorld* World = GetWorld();
	FTransform SpawnLocAndRotation(FRotator(0, 0, 0), FVector(90, 10, 120), FVector(10, 10, 10));
	AVRControl_Button* control = World->SpawnActorDeferred<AVRControl_Button>(AVRControl_Button::StaticClass(), SpawnLocAndRotation);
	auto type = EButtonType::Btn_Toggle_Rotate;
	auto inter = EControlInteractibleAxis::Axis_Y;


	control->Init(mesh, SpawnLocAndRotation, type, inter, 3, 2, 150, 0.2, false, true);
	control->FinishSpawning(SpawnLocAndRotation);


	/*MyActor->MySetupFunction(...);
	MyActor->FinishSpawning(SpawnLocAndRotation);*/
	
}

// Called every frame
void AVRControlSpawnTester::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

