// Fill out your copyright notice in the Description page of Project Settings.

#include "VRControl_Button.h"

#include <Components/StaticMeshComponent.h>
#include <GameFramework/Actor.h>
#include "DrawDebugHelpers.h"
AVRControl_Button::AVRControl_Button()
{
	DepressDistance = 15.0f;
	LastToggleTime = 0.0f;
	ButtonEngageDepth = 13.0f;
	DepressSpeed = 150.0f;
	MinTimeBetweenEngaging = 0.1f;
	bIsEnabled = true;
	InteractionAxis = FVector(0, 1, 0);
	RotationAxis = FVector(1, 0, 0);
	buttonType = EButtonType::Btn_Toggle_Return;
	maxAngle = 40.0f;
	RootComponent = mesh;
	//SetMeshFromPath("StaticMesh'/Game/VRControlsContent/ButtonSwitch/switch.switch_button-proste_osie'");
	//UE_LOG(LogTemp, Warning, TEXT("AVRControl_Button::Constructor"));
	FVector vec = -(ButtonEngageDepth + (1.e-2f)) * RotationAxis;
	vec = vec / ButtonEngageDepth * maxAngle;
	rotToSt = AVRControl::FVectorToFrotator(vec);
}
//FTransform, mesh, ButtonType, interactionAxis, DepressDistance, ButtonEngageDepth, DepressSpeed, MinTimeBetweenEngaging, IsEnabled, IsPressed
void AVRControl_Button::Init(UStaticMesh* _mesh, FTransform initialTransform,
	EButtonType buttonType, EControlInteractibleAxis interactionAxis,
	float depressDistance, float buttonEngageDepth, float depressSpeed,
	float minTimeBetweenEngaging, bool isPressed, bool isEnabled)
{
	InitBase(initialTransform, _mesh, interactionAxis, isEnabled);
	DepressDistance = depressDistance;
	LastToggleTime = 0.0f;
	ButtonEngageDepth = buttonEngageDepth;
	DepressSpeed = depressSpeed;
	MinTimeBetweenEngaging = minTimeBetweenEngaging;
	bIsEnabled = true;
	ButtonState = isPressed;
	this->buttonType = buttonType;
	//TODO if buttonPressed set to press state
	maxAngle = 40.0f;
	FVector vec = -(ButtonEngageDepth + (1.e-2f)) * RotationAxis;
	vec = vec / ButtonEngageDepth * maxAngle;
	rotToSt = AVRControl::FVectorToFrotator(vec);
	RotationAxis = FVector(1, 0, 0);
}
void AVRControl_Button::Init(FTransform initialTransform, UProceduralMeshComponent* _mesh,
	EButtonType buttonType, EControlInteractibleAxis interactionAxis,
	float depressDistance, float buttonEngageDepth, float depressSpeed,
	float minTimeBetweenEngaging, bool isPressed, bool isEnabled)
{
	//UE_LOG(LogTemp, Warning, TEXT("AVRControl_Button::InitProcedural"));
	InitBase(initialTransform, _mesh, interactionAxis, isEnabled);
	DepressDistance = depressDistance;
	LastToggleTime = 0.0f;
	ButtonEngageDepth = buttonEngageDepth;
	DepressSpeed = depressSpeed;
	MinTimeBetweenEngaging = minTimeBetweenEngaging;
	bIsEnabled = true;
	ButtonState = isPressed;
	this->buttonType = buttonType;
	//TODO if buttonPressed set to press state
	maxAngle = 40.0f;
	FVector vec = -(ButtonEngageDepth + (1.e-2f)) * RotationAxis;
	vec = vec / ButtonEngageDepth * maxAngle;
	rotToSt = AVRControl::FVectorToFrotator(vec);
	RotationAxis = FVector(1, 0, 0);
}
//FTransform, mesh, ButtonType, interactionAxis, rotationAxis, maxAngle, DepressDistance, ButtonEngageDepth, DepressSpeed, MinTimeBetweenEngaging, IsEnabled, IsPressed
void AVRControl_Button::Init(UStaticMesh* _mesh, FTransform initialTransform,
	EButtonType buttonType, EControlInteractibleAxis interactionAxis, EControlInteractibleAxis rotationAxis, float maxAngle,
	float depressDistance, float buttonEngageDepth, float depressSpeed,
	float minTimeBetweenEngaging, bool isPressed, bool isEnabled)
{
	InitBase(initialTransform, _mesh, interactionAxis, isEnabled);
	DepressDistance = depressDistance;
	LastToggleTime = 0.0f;
	ButtonEngageDepth = buttonEngageDepth;
	DepressSpeed = depressSpeed;
	MinTimeBetweenEngaging = minTimeBetweenEngaging;
	bIsEnabled = true;
	ButtonState = isPressed;
	this->buttonType = buttonType;

	SetRotationAxis(rotationAxis);
	this->maxAngle = maxAngle;
	FVector vec = -(ButtonEngageDepth + (1.e-2f)) * RotationAxis;
	vec = vec / ButtonEngageDepth * maxAngle;
	rotToSt = AVRControl::FVectorToFrotator(vec);
}

void AVRControl_Button::SetButtonToState(bool state)
{
	if (state)
	{
		switch (buttonType)
		{
		case EButtonType::Btn_Toggle_Stay:
		{
			mesh->SetRelativeLocation(InitialRelativeTransform.TransformPosition((-ButtonEngageDepth) * InteractionAxis), false);
			break;
		}
		case EButtonType::Btn_Toggle_Rotate:
		{
			mesh->SetRelativeRotation(InitialRelativeTransform.TransformRotation(rotToSt.Quaternion()));
			maxAngle *= -1;
			auto vec = -(ButtonEngageDepth + (1.e-2f)) * RotationAxis;
			vec = vec / ButtonEngageDepth * maxAngle;
			rotToSt = AVRControl::FVectorToFrotator(vec);
			ResetControlLocation();
			break;
		}
		}
	}
}

void AVRControl_Button::BeginPlay()
{
	Super::BeginPlay();
	SetButtonToState(ButtonState);
}

void AVRControl_Button::Tick(float DeltaTime)
{
	float posDiff = 0.0f;

	//FVector xVec = 300 * mesh->GetComponentTransform().TransformVector(FVector(1, 0, 0));
	//FVector yVec = 300 * mesh->GetComponentTransform().TransformVector(FVector(0, 1, 0));
	//FVector zVec = 300 * mesh->GetComponentTransform().TransformVector(FVector(0, 0, 1));

	//DrawDebugLine(
	//	GetWorld(),
	//	mesh->GetComponentTransform().GetLocation(),
	//	mesh->GetComponentTransform().GetLocation()  + xVec,
	//	FColor(255, 0, 0),
	//	false, -1, 0,
	//	5
	//);
	//DrawDebugLine(
	//	GetWorld(),
	//	mesh->GetComponentTransform().GetLocation(),
	//	mesh->GetComponentTransform().GetLocation() + yVec,
	//	FColor(0, 255, 0),
	//	false, -1, 0,
	//	5
	//);
	//DrawDebugLine(
	//	GetWorld(),
	//	mesh->GetComponentTransform().GetLocation(),
	//	mesh->GetComponentTransform().GetLocation() + zVec,
	//	FColor(0, 0, 255),
	//	false, -1, 0,
	//	5
	//);
	if (!bIsEnabled)
	{
		if (InteractingComponent.IsValid())
			InteractingComponent.Reset();
		this->SetActorTickEnabled(false);
	}
	if (InteractingComponent.IsValid())
	{
		posDiff = FMath::Clamp(GetDifferenceOnInteractionAxis(), 0.0f, DepressDistance);
		InterpretPositionDifferenceOnAxis(posDiff);
	}
	else InterpolateToCorrectState(DeltaTime);
	CheckIfStateShouldChange(posDiff);
}

void AVRControl_Button::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	auto act = GetAttachParentActor();
	if (act != nullptr) //fix for button flickering when changing scale
	{
		auto vec = act->GetActorRelativeScale3D();
		//auto vec1 = act->GetActorScale3D();
		//UE_LOG(LogTemp, Warning, TEXT("Scale: %s, %s"), *vec.ToString(),*vec1.ToString());
		//InitialRelativeTransform.SetScale3D(vec);
	}
	if (!bIsEnabled) return;
	if (!InteractingComponent.IsValid() && IsValidOverlap(OtherComp))
	{
		Super::OnOverlapBegin(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
		OnTouchBegin.Broadcast();
		InteractingComponent = OtherComp;
		FTransform OriginalBaseTransform = InitialRelativeTransform;
		FVector loc = InteractingComponent->GetComponentLocation();
		InitialOtherLocation = OriginalBaseTransform.InverseTransformPosition(loc); // w ukladzie buttona reka
		InitialThisLocation = OriginalBaseTransform.InverseTransformPositionNoScale(mesh->RelativeLocation);  // w ukladzie do parenta pozycja buttona		
		if (buttonType == EButtonType::Btn_Toggle_Rotate)
		{
			auto thirdAxis = FVector::CrossProduct(RotationAxis, InteractionAxis);
			float whereOnThird = FVector::DotProduct(thirdAxis * thirdAxis, InitialOtherLocation);
			if (whereOnThird < 0 && !ButtonState || whereOnThird > 0 && ButtonState)
			{
				return;
			}
		}
		initialPositionOnAxis = FVector::DotProduct(InitialThisLocation, InteractionAxis);
		this->SetActorTickEnabled(true);
		bToggledThisTouch = false;
	}
}

void AVRControl_Button::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (InteractingComponent.IsValid() && OtherComp == InteractingComponent)
	{
		Super::OnOverlapEnd(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex);
		OnTouchEnd.Broadcast();
		InteractingComponent.Reset();
	}
}

float AVRControl_Button::GetMinPositionOnInteractionAxis()
{
	float currScale = FVector::DotProduct(InteractionAxis, InitialRelativeTransform.GetScale3D());
	if (ButtonState && buttonType == EButtonType::Btn_Toggle_Stay)
		return -((ButtonEngageDepth + (1.e-2f))*currScale);
	else if (bToggledThisTouch && buttonType == EButtonType::Btn_Toggle_Rotate)
		return -(ButtonEngageDepth + (1.e-2f));
	return 0.0f;
}

void AVRControl_Button::InterpretPositionDifferenceOnAxis(float posDiff)
{
	if (posDiff <= 0.0f) return;
	float ClampMinDepth = GetMinPositionOnInteractionAxis();	
	currentPositionOnAxis = FMath::Clamp(initialPositionOnAxis - posDiff, -DepressDistance, ClampMinDepth);	
	switch (buttonType)
	{
	case EButtonType::Btn_Toggle_Return:
	case EButtonType::Btn_Toggle_Stay:
	case EButtonType::Btn_Press:
	{
		mesh->SetRelativeLocation(InitialRelativeTransform.TransformPositionNoScale(currentPositionOnAxis * InteractionAxis), false);
		break;
	}
	case EButtonType::Btn_Toggle_Rotate:
	{
		FVector vec = (currentPositionOnAxis / ButtonEngageDepth * maxAngle) * RotationAxis;
		mesh->SetRelativeRotation(InitialRelativeTransform.TransformRotation(AVRControl::FVectorToFrotator(vec).Quaternion()));
		break;
	}
	}
}

void AVRControl_Button::CheckIfStateShouldChange(float posDiff)
{
	const float WorldTime = GetWorld()->GetTimeSeconds();
	switch (buttonType)
	{
	case EButtonType::Btn_Toggle_Return:
	case EButtonType::Btn_Toggle_Stay:
	case EButtonType::Btn_Toggle_Rotate:
	{
		if (!InteractingComponent.IsValid() || posDiff <= 0.0f) return;
		if (!bToggledThisTouch && currentPositionOnAxis <= (-ButtonEngageDepth) + KINDA_SMALL_NUMBER && (WorldTime - LastToggleTime) >= MinTimeBetweenEngaging)
			ChangeButtonState(!ButtonState, WorldTime);
		break;
	}
	case EButtonType::Btn_Press:
	{
		bool bCheckState = (FVector::DotProduct(InitialRelativeTransform.InverseTransformPositionNoScale(mesh->RelativeLocation), InteractionAxis) <= (-ButtonEngageDepth) + KINDA_SMALL_NUMBER);
		if (ButtonState != bCheckState && (WorldTime - LastToggleTime) >= MinTimeBetweenEngaging)
			ChangeButtonState(bCheckState, WorldTime);
		break;
	}
	}
}

void AVRControl_Button::ChangeButtonState(bool newState, float timeOfChange)
{
	LastToggleTime = timeOfChange;
	bToggledThisTouch = true;
	ButtonState = newState;
	bControlValue = ButtonState;
	OnStateChanged.Broadcast(this);
}

void AVRControl_Button::InterpolateToCorrectState(float deltaTime)
{
	switch (buttonType)
	{
	case EButtonType::Btn_Toggle_Return:
	case EButtonType::Btn_Toggle_Stay:
	case EButtonType::Btn_Press:
	{		
		auto dist = (mesh->RelativeLocation - InitialRelativeTransform.GetTranslation()).Size();
		if (FMath::IsNearlyZero(dist,0.001f))
		{
			this->SetActorTickEnabled(false);
			InteractingComponent.Reset();
		}
		else
		{
			auto vec = (buttonType == EButtonType::Btn_Toggle_Stay && ButtonState) ?
				InitialRelativeTransform.TransformPosition(InteractionAxis * (-(ButtonEngageDepth + (1.e-2f))))
				: InitialRelativeTransform.GetTranslation();
			mesh->SetRelativeLocation(FMath::VInterpConstantTo(mesh->RelativeLocation, vec, deltaTime, DepressSpeed), false);
		}
		break;
	}
	case EButtonType::Btn_Toggle_Rotate:
	{
		if ((mesh->RelativeRotation - InitialRelativeTransform.Rotator()).Equals(rotToSt))
		{
			this->SetActorTickEnabled(false);
			InteractingComponent.Reset();
			maxAngle *= -1;
			ResetControlLocation();
			FVector vec = -(ButtonEngageDepth + (1.e-2f)) * RotationAxis;
			vec = vec / ButtonEngageDepth * maxAngle;
			rotToSt = AVRControl::FVectorToFrotator(vec);
			currentPositionOnAxis = 0.0f;
		}
		else
		{
			auto currentRotation = FMath::Abs(currentPositionOnAxis) > (ButtonEngageDepth + (1.e-2f)) ?
				InitialRelativeTransform.TransformRotation(rotToSt.Quaternion())
				: InitialRelativeTransform.GetRotation();
			mesh->SetRelativeRotation(FMath::RInterpConstantTo(mesh->RelativeRotation, currentRotation.Rotator(), deltaTime, DepressSpeed), false);
		}
		break;
	}
	}
}


float AVRControl_Button::GetDifferenceOnInteractionAxis()
{
	auto handInLocal = InitialRelativeTransform.InverseTransformPosition(InteractingComponent->GetComponentLocation());
	auto vec = InitialOtherLocation - handInLocal; //in global when attached, because RelativeTransform is not GlobalTransform
	auto vecInLocal = mesh->GetComponentTransform().InverseTransformVector(vec);
	auto dot = FVector::DotProduct(vecInLocal, InteractionAxis);
	return dot;
}