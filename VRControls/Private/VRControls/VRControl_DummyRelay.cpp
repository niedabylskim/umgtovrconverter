// Fill out your copyright notice in the Description page of Project Settings.

#include "VRControl_DummyRelay.h"


AVRControl_DummyRelay::AVRControl_DummyRelay()
{
	//InputCount = 1;
	//OutputCount = 1;

	InputsDictionary.Add("Input1", 0);		 //TODO: move to cfg
	OutputsDictionary.Add("Output1", 0);	 //TODO: move to cfg

}

float AVRControl_DummyRelay::GetOutput(int OutputIdx) const
{
	if (OutputIdx == 0)
		return value;

	return nanf("");
}

void AVRControl_DummyRelay::SetInput(int InputIdx, float Value)
{
	if (InputIdx == 0)
		this->value = Value;
	else
		throw std::exception("No specified input");
}

void AVRControl_DummyRelay::BeginPlay()
{
	value = 0.0f;
}

void AVRControl_DummyRelay::Tick(float DeltaTime)
{
	UE_LOG(LogTemp, Warning, TEXT("DummyRelay[%s]: %f"), *GetName(), value);
}
