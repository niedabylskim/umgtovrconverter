// Fill out your copyright notice in the Description page of Project Settings.

#include "VRControl_Indicator.h"
#include <Runtime/Engine/Classes/Engine/StaticMesh.h>
#include <Runtime/Engine/Classes/Components/TextRenderComponent.h>

// Sets default values
AVRControl_Indicator::AVRControl_Indicator()
{
	InitialRelativeTransform = FTransform::Identity;
	maxAngle = 45.0f;
	minAngle = -45.0f;
	minValue = 8;
	maxValue = 16;
	RotationSpeed = 50;

	InputCount = 1;
	OutputCount = 0;
	InputsDictionary.Add("In", 0); //TODO: move to cfg
}

void AVRControl_Indicator::Init(UStaticMesh *_mesh, FTransform initialTransform, EControlInteractibleAxis interactionAxis,
	float minAngle, float maxAngle, float minValue, float maxValue, float rotationSpeed, bool isEnabled)
{
	InitBase(initialTransform, _mesh, interactionAxis, isEnabled);
	this->minAngle = minAngle;
	this->maxAngle = maxAngle;
	this->minValue = minValue;
	this->maxValue = maxValue;
	this->RotationSpeed = rotationSpeed;
}

// Called when the game starts or when spawned
void AVRControl_Indicator::BeginPlay()
{
	Super::BeginPlay();
	SetupInput();
	Text = GetWorld()->SpawnActor<ATextRenderActor>(ATextRenderActor::StaticClass(), GetActorLocation() + FVector(0, 1, 0), GetActorRotation() + FRotator(0, 90, 0));
	Text->GetTextRender()->SetTextRenderColor(FColor::Red);
	Text->SetActorScale3D(FVector::OneVector);
	Text->GetTextRender()->SetWorldSize(3);
	Text->GetTextRender()->SetHorizontalAlignment(EHorizTextAligment::EHTA_Center);
	SetIndicatorValue(12);
}

// Called every frame
void AVRControl_Indicator::Tick(float DeltaTime)
{
	rotation = FMath::RInterpConstantTo(rotation.Rotator(), dstRotation.Rotator(), DeltaTime, RotationSpeed).Quaternion();
	//TODO other interpolation type
	mesh->SetRelativeRotation(rotation.Rotator());
	
	float dist = mesh->RelativeRotation.Quaternion().AngularDistance(dstRotation);

	if (FMath::IsNearlyZero(dist))
	{
		mesh->SetRelativeRotation(dstRotation);
		this->SetActorTickEnabled(false);
	}
}

void AVRControl_Indicator::SetInput(int InputIdx, float Value)
{
	if (InputIdx >= 0 && InputIdx < InputCount)
	{
		if (InputIdx == 0)
			SetIndicatorValue(Value);
	}
}

void AVRControl_Indicator::SetIndicatorValue(float value)
{
	if (value < minValue || value > maxValue)
	{
		UE_LOG(LogTemp, Error, TEXT("VRCIndicator::out of bounds"));
		return;
	}
	Text->GetTextRender()->SetText(GetFloatAsTextWithPrecision(value,2,false));
	value -= minValue;
	float maxV = maxValue - minValue;
	float percent = value / maxV;
	float dstAngle = (maxAngle - minAngle) * percent;
	float angleToMove = maxAngle - dstAngle;
	//TODO check interactionAxis
	dstRotation = FQuat(FRotator(angleToMove, 0, 0));
	this->SetActorTickEnabled(true);
}

void AVRControl_Indicator::SetupInput()
{
	EnableInput(GetWorld()->GetFirstPlayerController());
	UInputComponent* myInputComp = this->InputComponent;
	if (!myInputComp) return;
	UE_LOG(LogTemp, Warning, TEXT("VRCIndicator::SetupInput"));
	InputComponent->BindAction("G", IE_Pressed, this, &AVRControl_Indicator::G_Pressed);
}

void AVRControl_Indicator::G_Pressed()
{
	if (mesh != NULL)
	{
		float random = ((float)rand()) / (float)RAND_MAX;
		float diff = maxValue - minValue;
		float r = random * diff;
		float a = minValue + r;
		//UE_LOG(LogTemp, Warning, TEXT("VRCIndicator::G_Pressed %f"),a);
		SetIndicatorValue(a);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("VRCIndicator::mesh == null"));
	}
}


//Float as FText With Precision!
FORCEINLINE FText AVRControl_Indicator::GetFloatAsTextWithPrecision(float TheFloat, int32 Precision, bool IncludeLeadingZero = true)
{
	//Round to integral if have something like 1.9999 within precision
	float Rounded = roundf(TheFloat);
	if (FMath::Abs(TheFloat - Rounded) < FMath::Pow(10, -1 * Precision))
	{
		TheFloat = Rounded;
	}
	FNumberFormattingOptions NumberFormat; //Text.h
	NumberFormat.MinimumIntegralDigits = (IncludeLeadingZero) ? 1 : 0;
	NumberFormat.MaximumIntegralDigits = 10000;
	NumberFormat.MinimumFractionalDigits = Precision;
	NumberFormat.MaximumFractionalDigits = Precision;
	return FText::AsNumber(TheFloat, &NumberFormat);
}