// Fill out your copyright notice in the Description page of Project Settings.

#include "VRControl_Knob.h"
#include <Components/StaticMeshComponent.h>

AVRControl_Knob::AVRControl_Knob()
{
	SetMeshFromPath("StaticMesh'/Game/VRControlsContent/Knob/DialIndicatorTop.DialIndicatorTop'");
	RotationAxis = FVector(1, 0, 0);
	InteractionAxis = FVector(0, 0, 1);
	SetRotationAxis(EControlInteractibleAxis::Axis_X);
	SetInteractionAxis(EControlInteractibleAxis::Axis_Z);
	IsAngleSnapped = true;
	SnapAngleThreshold = 3.0f;
	SnapAngleIncrement = 5.0f;
	lastAngle = 0.0f;
	RotationScaler = -1.0f;;
	MaxAngle = 360.0f;
	MinAngle = 50.0f;
	CurrentAngle = 130;
	BreakDistance = 50.0f;
}

//FTransform, mesh, interactionAxis, rotationAxis, maxAngle, 
//isEnabled, currentAngle, BreakDistance, RotationScaler, snapAngles, SnapAngleThreshold, SnapAngleIncrement

void AVRControl_Knob::Init(UStaticMesh* _mesh, FTransform initialTransform,
	EControlInteractibleAxis interactionAxis, EControlInteractibleAxis rotationAxis, float minAngle, float maxAngle,
	float currentAngle, float breakDistance, float rotationScaler, bool snapAngles,
	float snapAngleIncrement, float snapAngleThreshold, bool isEnabled)
{
	InitBase(initialTransform, _mesh, interactionAxis, isEnabled);
	SetRotationAxis(rotationAxis);
	IsAngleSnapped = snapAngles;
	SnapAngleThreshold = snapAngleThreshold;
	this->SnapAngleIncrement = snapAngleIncrement;
	lastAngle = currentAngle;
	RotationScaler = rotationScaler;
	CurrentAngle = currentAngle;
	MinAngle = minAngle;
	MaxAngle = maxAngle;
	BreakDistance = breakDistance;
}

void AVRControl_Knob::BeginPlay()
{
	Super::BeginPlay();
}

void AVRControl_Knob::ResetControlLocation()
{
	Super::ResetControlLocation();	
	lastAngle = CurrentAngle - SnapAngleThreshold - 1;
	AddAngle(0);
}

void AVRControl_Knob::OnGripTick_Implementation(UVRGripper *VRGripper, float DeltaTime)
{
	if (!bIsEnabled || CheckIfGripShouldBreak(VRGripper))
	{
		VRGripper->DropGrippedActor(this);
		return;
	}
	FRotator curRotation = VRGripper->GetComponentQuat().Rotator();
	FVector vec = AVRControl::FRotatorToFVector((curRotation - LastRotation).GetNormalized());
	float DeltaRot = RotationScaler * FVector::DotProduct(vec, RotationAxis);
	LastRotation = curRotation;
	if (FMath::Abs(DeltaRot) < 0.1f) return;
	AddAngle(DeltaRot);
}

void AVRControl_Knob::OnGrip_Implementation(UVRGripper *VRHandController)
{
	if (!bIsEnabled) return;
	FTransform CurrentRelativeTransform = InitialRelativeTransform;
	FTransform ReversedRelativeTransform = FTransform(VRHandController->GetComponentTransform().ToInverseMatrixWithScale());
	FTransform RelativeToGripTransform = ReversedRelativeTransform * this->GetActorTransform();
	InitialDropLocation = mesh->GetComponentTransform().InverseTransformPosition(VRHandController->GetComponentLocation());
	LastRotation = VRHandController->GetComponentQuat().Rotator();
}

void AVRControl_Knob::OnGripRelease_Implementation(UVRGripper *VRGripper)
{
	if (!bIsEnabled) return;
	if (IsAngleSnapped && FMath::Abs(FMath::Fmod(CurrentAngle, SnapAngleIncrement)) <= FMath::Min(SnapAngleIncrement, SnapAngleThreshold))
	{
		FRotator rotation = AVRControl::FVectorToFrotator(FMath::GridSnap(CurrentAngle, SnapAngleIncrement) * InteractionAxis);
		this->SetActorRelativeRotation((FTransform(rotation) * InitialRelativeTransform).Rotator());
		CurrentAngle = FMath::GridSnap(CurrentAngle, SnapAngleIncrement);
		CurrentAngle = FRotator::ClampAxis(FMath::RoundToFloat(CurrentAngle));
	}
}

void AVRControl_Knob::AddAngle(float deltaAngle)
{
	ClampAngleToBoundaries(deltaAngle);
	float delta = CurrentAngle - lastAngle;
	if (IsAngleSnapped && FMath::Abs(delta) > SnapAngleThreshold)
	{
		float angleThisFrame = FMath::GridSnap(CurrentAngle, SnapAngleIncrement);
		UpdateMeshRotaion(angleThisFrame);
	}
	else if (!IsAngleSnapped)
		UpdateMeshRotaion(CurrentAngle);
}

void AVRControl_Knob::ClampAngleToBoundaries(float deltaAngle)
{
	float tempCheck = FRotator::ClampAxis(CurrentAngle + deltaAngle);
	if (MinAngle < MaxAngle)
	{
		auto angleAfterClamp = FMath::Clamp(tempCheck, MinAngle, MaxAngle);
		
		if (deltaAngle < 0.0f)
			CurrentAngle = angleAfterClamp <= CurrentAngle ? angleAfterClamp : MinAngle;
		else
			CurrentAngle = angleAfterClamp >= CurrentAngle ? angleAfterClamp : MaxAngle;
	}
	else CurrentAngle = FMath::ClampAngle(tempCheck, MinAngle, MaxAngle);
}
void AVRControl_Knob::UpdateMeshRotaion(float angle)
{
	FRotator rot = AVRControl::FVectorToFrotator(angle * InteractionAxis);
	this->SetActorRelativeRotation((FTransform(rot) * InitialRelativeTransform).Rotator());
	CurrentAngle = FMath::RoundToFloat(angle);
	bControlValue = FRotator::ClampAxis(CurrentAngle);
	if (!FMath::IsNearlyEqual(lastAngle, CurrentAngle))
		OnStateChanged.Broadcast(this);
	lastAngle = CurrentAngle;
	UE_LOG(LogTemp, Warning, TEXT("knob %s: %f"), *GetName(), bControlValue);

}