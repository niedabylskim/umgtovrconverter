// Fill out your copyright notice in the Description page of Project Settings.

#include "VRControl_Lever.h"

#include <Components/StaticMeshComponent.h>


#include <DrawDebugHelpers.h>

AVRControl_Lever::AVRControl_Lever()
{
	SetMeshFromPath("StaticMesh'/Game/VRControlsContent/Lever/LeverTop.LeverTop'");
	InitialRelativeTransform = FTransform::Identity;
	InitialInteractorLocation = FVector::ZeroVector;
	snapAngles = AVRControl::FVectorToFrotator(FVector(5, 5, 5));
	HasDiscreteStates = false;
	IsReturningToZero = false;
	ReturningSpeed = 20.0f;
	maxAngle = 40.0f;
	minAngle = -40.0f;
	BreakDistance = 50.0f;
}

void AVRControl_Lever::Init(UStaticMesh *_mesh, FTransform initialTransform, EControlInteractibleAxis interactionAxis,
	float minAngle, float maxAngle, float startingAngle, bool hasDiscreteStates, float snapAngle,
	bool isReturningToZero, float returningSpeed, float breakGripDistance, bool isEnabled)	
{
	InitBase(initialTransform, _mesh, interactionAxis, isEnabled);
	this->minAngle = minAngle;
	this->maxAngle = maxAngle;
	this->IsReturningToZero = isReturningToZero;
	this->ReturningSpeed = ReturningSpeed;
	this->snapAngle = snapAngle;
	this->snapAngles = AVRControl::FVectorToFrotator(FVector(snapAngle, snapAngle, snapAngle));
	this->HasDiscreteStates = hasDiscreteStates;
	this->BreakDistance = breakGripDistance;
}

void AVRControl_Lever::BeginPlay()
{
	Super::BeginPlay();
}

void AVRControl_Lever::Tick(float DeltaTime)
{
	FRotator currentRotation;
	onreleasegrapQuaternion = FMath::RInterpConstantTo(onreleasegrapQuaternion.Rotator(), startQuaternion.Rotator(), DeltaTime, ReturningSpeed).Quaternion();
	FRotator quaternionRotator = onreleasegrapQuaternion.Rotator();
	currentRotation = HasDiscreteStates ? quaternionRotator.GridSnap(snapAngles) : quaternionRotator;
	mesh->SetRelativeRotation(currentRotation);
	float dist = mesh->RelativeRotation.Quaternion().AngularDistance(startQuaternion);
	
	if (FMath::IsNearlyZero(dist))
	{
		mesh->SetRelativeRotation(startQuaternion);
		this->SetActorTickEnabled(false);
	}
}

void AVRControl_Lever::ResetControlLocation()
{
	Super::ResetControlLocation();
	SetStartingAngle(startingAngle);
}

void AVRControl_Lever::SetStartingAngle(float angle)
{
	if (minAngle > maxAngle)
		Swap(minAngle, maxAngle);

	angle = FMath::Clamp(angle, minAngle, maxAngle);
	
	if (interAxis == EControlInteractibleAxis::Axis_XY)
	{
		UE_LOG(LogTemp, Error, TEXT("Not implemented yet!!!"));
		return;
	}
	auto vec = AVRControl::FVectorToFrotator(AVRControl::FRotatorToFVector(FRotator(angle, angle, angle)) * InteractionAxis);
	mesh->SetRelativeRotation(mesh->RelativeRotation.Quaternion() * vec.Quaternion());
	startQuaternion = mesh->RelativeRotation.Quaternion(); // poczatkowy kwaternion w lokalnym ukladzie
}

void AVRControl_Lever::OnGripTick_Implementation(UVRGripper *VRGripper, float DeltaTime)
{
	if (!bIsEnabled || CheckIfGripShouldBreak(VRGripper))
	{
		VRGripper->DropGrippedActor(this);
		return;
	}
	auto diffVec = VRGripper->GetComponentLocation() - InitialRelativeTransform.GetLocation(); //vec od poczatku do reki
	auto gripRelative = InitialRelativeTransform.GetRotation().UnrotateVector(diffVec); // wektor wyrazony w poczatkowym ukladzie levera
	FQuat quat = FQuat::FindBetweenVectors(rotationAtGrabVector, gripRelative); // kat pomiedzy poczatkowym a obecnym polozeniem w ukladzie lokalnym
	FRotator quatRot = quat.Rotator(); // rotator do clampowania i dyskretnych stanow
	if (HasDiscreteStates) quatRot = quatRot.GridSnap(snapAngles);
	auto vectmp = AVRControl::FRotatorToFVector(quatRot) * InteractionAxis;
	quatRot = AVRControl::FVectorToFrotator(vectmp);
	FQuat q = quatLev * quatRot.Quaternion();
	q = ClampAngle(q);		
	mesh->SetRelativeRotation(q);
}

FQuat AVRControl_Lever::ClampAngle(FQuat q)
{
	auto diff = AVRControl::FRotatorToFVector(InitialRelativeTransform.Rotator());
	auto quatRot = AVRControl::FRotatorToFVector(q.Rotator()); // wektor odpowiadajacy rotatorowi
	//TODO: clamp based on angular distance
	switch (interAxis)
	{
	case EControlInteractibleAxis::Axis_X:
	{	float min = minAngle + diff.X;
		float max = maxAngle + diff.X;
		quatRot.X = FMath::Clamp(quatRot.X, min, max);
		break;
	}
	case EControlInteractibleAxis::Axis_Y:
	{	float min = minAngle + diff.Y;
		float max = maxAngle + diff.Y;
		quatRot.Y = FMath::Clamp(quatRot.Y, min, max);
		break;
	}
	case EControlInteractibleAxis::Axis_Z:
	{
		float min = minAngle + diff.Z;
		float max = maxAngle + diff.Z;
		quatRot.Z = FMath::Clamp(quatRot.Z, min, max);
		break;
	}
	case EControlInteractibleAxis::Axis_XY:
	{
		float minX = minAngle + diff.X;
		float maxX = maxAngle + diff.X;
		float minY = minAngle + diff.Y;
		float maxY = maxAngle + diff.Y;
		float minZ = minAngle + diff.Z;
		float maxZ = maxAngle + diff.Z;
		quatRot.X = FMath::Clamp(quatRot.X, minX, maxX);
		quatRot.Y = FMath::Clamp(quatRot.Y, minY, maxY);
		quatRot.Z = FMath::Clamp(quatRot.Z, minZ, maxZ);
		break;
	}
	}
	
	bControlValue = FVector::DotProduct(quatRot-diff, InteractionAxis);
	UE_LOG(LogTemp, Warning, TEXT("d: %f"), bControlValue);
	OnStateChanged.Broadcast(this);

	return AVRControl::FVectorToFrotator(quatRot).Quaternion();
}

void AVRControl_Lever::OnGripRelease_Implementation(UVRGripper *VRGripper)
{
	if (!IsReturningToZero) return;
	this->SetActorTickEnabled(true);
	onreleasegrapQuaternion = mesh->RelativeRotation.Quaternion();
}

void AVRControl_Lever::OnGrip_Implementation(UVRGripper *VRGripper)
{
	if (!bIsEnabled) return;
	InitialDropLocation = mesh->GetComponentTransform().InverseTransformPosition(VRGripper->GetComponentLocation());
	if (interAxis == EControlInteractibleAxis::Axis_XY)
	{
		rotationAtGrabVector = InitialDropLocation;
		quatLev = InitialRelativeTransform.GetRotation();
	}
	else
	{
		rotationAtGrabVector = InitialRelativeTransform.GetRotation().UnrotateVector(VRGripper->GetComponentLocation() - InitialRelativeTransform.GetLocation());
		quatLev = mesh->RelativeRotation.Quaternion(); // poczatkowy kwaternion w lokalnym ukladzie
	}

	this->SetActorTickEnabled(false);
}