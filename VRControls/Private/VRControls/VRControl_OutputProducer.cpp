// Fill out your copyright notice in the Description page of Project Settings.

#include "VRControl_OutputProducer.h"

AVRControl_OutputProducer::AVRControl_OutputProducer()
{
	OutputValue = 12.0f;
	PrimaryActorTick.bStartWithTickEnabled = true;

	InputCount = 0;
	OutputCount = 1;
	OutputsDictionary.Add("Voltage", 0);	 //TODO: move to cfg
}

void AVRControl_OutputProducer::BeginPlay()
{
	Super::BeginPlay();
}

void AVRControl_OutputProducer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	static int counter = 100;
	if (counter-- <= 0)
	{
		int sign = FMath::RandBool() ? 4 : -4;
		float tmpValue = OutputValue + sign * FMath::FRand();
		bControlValue = tmpValue;
		OnStateChanged.Broadcast(this);
		//UE_LOG(LogTemp, Warning, TEXT("output: %f"), tmpValue);
		counter = 100;
	}
}

void AVRControl_OutputProducer::Init(float outputValue)
{
	OutputValue = outputValue;
}

float AVRControl_OutputProducer::GetOutput(int OutputIdx) const
{
	if (OutputIdx >= 0 && OutputIdx < OutputCount)
	{
		if (OutputIdx == 0)
			return bControlValue;
	}

	return nanf("");
}
