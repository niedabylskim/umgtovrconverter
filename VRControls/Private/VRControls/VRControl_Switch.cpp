// Fill out your copyright notice in the Description page of Project Settings.

#include "VRControl_Switch.h"
#include <Components/StaticMeshComponent.h>
AVRControl_Switch::AVRControl_Switch()
{
	SetMeshFromPath("StaticMesh'/Game/VRControlsContent/Lever/LeverTop.LeverTop'");
	maxAngle = 45;
	minAngle = -45;
	numberOfStates = 2;
	angleStep = (maxAngle - minAngle) / (numberOfStates - 1);
	currentState = 0;
	RotationAxis = FVector(0, 1, 0);
	InteractionAxis = FVector(1, 0, 0);
	rotAxis = EControlInteractibleAxis::Axis_Y;
	interAxis = EControlInteractibleAxis::Axis_X;
	speedThreshold = 30;
	minTimeBetweenCollision = 0.2f;
}

void AVRControl_Switch::Init(UStaticMesh* _mesh, FTransform initialTransform,
	EControlInteractibleAxis interactionAxis, EControlInteractibleAxis rotationAxis,
	float minAngle, float maxAngle, float numOfStates, float currentState,
	float minSpeedToInteract, float minTimeBetweenCollision, bool isEnabled)

{
	InitBase(initialTransform, _mesh, interactionAxis, isEnabled);
	this->maxAngle = maxAngle;
	this->minAngle = minAngle;
	numberOfStates = numOfStates;
	angleStep = (maxAngle - minAngle) / (numberOfStates - 1);
	this->currentState = currentState;
	SetRotationAxis(rotationAxis);
	speedThreshold = minSpeedToInteract;
	this->minTimeBetweenCollision = minTimeBetweenCollision;
}

//FTransform, mesh, interactionAxis, rotationAxis, maxAngle, minAngle, numOfStates, currentState

void AVRControl_Switch::BeginPlay()
{
	Super::BeginPlay();
	angleStep = (maxAngle - minAngle) / (numberOfStates - 1);
	//ChangeSwitchState(currentState);
}

void AVRControl_Switch::Tick(float DeltaTime)
{
	if (!bIsEnabled) return;
	const float WorldTime = GetWorld()->GetTimeSeconds();
	if (WorldTime - lastSwitchTime <= minTimeBetweenCollision) return;
	if (!InteractingComponent.IsValid()) return;
	InterpretVelocity(WorldTime);
}

void AVRControl_Switch::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!bIsEnabled) return;
	const float WorldTime = GetWorld()->GetTimeSeconds();
	if (WorldTime - lastSwitchTime <= minTimeBetweenCollision) return;
	if (!InteractingComponent.IsValid() && IsValidOverlap(OtherComp))
	{
		InteractingComponent = OtherComp;
		FTransform OriginalBaseTransform = InitialRelativeTransform;
		FVector loc = InteractingComponent->GetComponentLocation();
		InitialOtherLocation = OriginalBaseTransform.InverseTransformPosition(loc); // reka w ukladzie buttona
		InitialThisLocation = OriginalBaseTransform.InverseTransformPosition(mesh->GetComponentLocation()); // pozycja buttona wzgledem parenta
		this->SetActorTickEnabled(true);
		InterpretVelocity(WorldTime);
	}
}

void AVRControl_Switch::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (InteractingComponent.IsValid() && OtherComp == InteractingComponent)
	{
		InteractingComponent.Reset();
	}
	this->SetActorTickEnabled(false);
}

void AVRControl_Switch::ChangeSwitchState(int stateNum)
{
	auto rotVec = AVRControl::FRotatorToFVector(InitialRelativeTransform.GetRotation().Rotator());
	int sign = FMath::Sign(stateNum - currentState);
	float angle = sign * angleStep;
	FVector vec = angle * RotationAxis;
	currentState = stateNum;
	mesh->SetRelativeRotation(mesh->RelativeRotation.Quaternion() * AVRControl::FVectorToFrotator(vec).Quaternion());
	bControlValue = stateNum;
	UE_LOG(LogTemp, Warning, TEXT("state: %d"), bControlValue);
	OnStateChanged.Broadcast(this);
}
void AVRControl_Switch::InterpretVelocity(float time)
{
	FVector vel = InteractingComponent->GetPhysicsLinearVelocity();
	auto initialVelocityOnAxis = FVector::DotProduct(InitialRelativeTransform.InverseTransformVector(vel), InteractionAxis);
	auto newState = currentState;
	if (initialVelocityOnAxis < -speedThreshold && currentState != numberOfStates - 1) newState++;
	else if (initialVelocityOnAxis > speedThreshold && currentState != 0) newState--;
	if (newState == currentState) return;
	ChangeSwitchState(newState);
	lastSwitchTime = time;
}

