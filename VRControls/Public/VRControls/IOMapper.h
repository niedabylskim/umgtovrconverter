// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class VRCONTROLS_API IOMapper
{
public:
	virtual ~IOMapper() {}

	//return number of control inputs
	int GetInputCount() const { return InputCount; }

	//return number of control outputs
	int GetOutputCount() const { return OutputCount; }

	//return value of output define by OutputName
	virtual float GetOutputValue(FString OutputName) const 
	{
		auto find = OutputsDictionary.Find(OutputName);
		if (find == nullptr)
			throw std::exception("No specified output");

		return GetOutput(*find);
	};

	//pass value of input defined by InputName
	virtual void SetInputValue(FString InputName, float Value)
	{
		auto find = InputsDictionary.Find(InputName);
		if (find == nullptr)
			throw std::exception("No specified input");

		SetInput(*find, Value);
	}

	TMap<FString, int> GetInputsDictionary() const { return InputsDictionary; }
	TMap<FString, int> GetOutputsDictionary() const { return OutputsDictionary; }

protected:
//private:
	int InputCount;
	int OutputCount;

	TMap<FString, int> InputsDictionary;

	TMap<FString, int> OutputsDictionary;

	//return value of output define by OutputIdx
	virtual float GetOutput(int OutputIdx) const = 0;

	//pass value of input defined by InputIdx
	virtual void SetInput(int InputIdx, float Value) = 0;

};
