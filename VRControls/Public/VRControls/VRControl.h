// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include <VRHumanInterfaceDevice/Public/VRHID/HandController/HandController.h>
#include <IOMapper.h>
#include <ProceduralMeshComponent.h>

#include <VRUtilities/Gripper/IVRGrippable.h>
#include <VRUtilities/Gripper/VRGripper.h>

#include "VRControl.generated.h"

UENUM()
enum class EControlInteractibleAxis : uint8
{
	Axis_X,
	Axis_Y,
	Axis_Z,
	Axis_XY
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FControlChangeState, UObject*, ControlValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnControlTouchBeginEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnControlTouchEndEvent);

UCLASS(abstract)
class VRCONTROLS_API AVRControl : public AActor, public IVRGrippable, public IOMapper
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	AVRControl();
	virtual ~AVRControl() {}
	void SetMesh(UStaticMesh* _mesh, FVector scale = FVector::OneVector);
	UPROPERTY()
	FControlChangeState OnStateChanged;
	UPROPERTY()
	FOnControlTouchEndEvent OnTouchBegin;
	UPROPERTY()
	FOnControlTouchEndEvent OnTouchEnd;
	void SetInteractionAxis(EControlInteractibleAxis newInteractionAxis);
	void SetRotationAxis(EControlInteractibleAxis newRotationAxis);
	float GetControlValue() { return bControlValue; }

#if WITH_EDITOR
	void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
	{
		FName PropertyName = (PropertyChangedEvent.Property != NULL) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

		if ((PropertyName == GET_MEMBER_NAME_CHECKED(AVRControl, interAxis)))
		{
			SetInteractionAxis(interAxis);

		}
		else if ((PropertyName == GET_MEMBER_NAME_CHECKED(AVRControl, rotAxis)))
		{
			SetRotationAxis(rotAxis);
		}
		Super::PostEditChangeProperty(PropertyChangedEvent);
	}
#endif
protected:
	/*Interaction axis*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = true, DisplayName = "Interaction axis"), Category = "VRControl")
	EControlInteractibleAxis interAxis;

	/*Rotation axis if applicable i.e Btn_Toggle_Rotate*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = true, DisplayName = "Rotation axis"), Category = "VRControl")
	EControlInteractibleAxis rotAxis;

	float bControlValue;
	FVector InteractionAxis;
	FVector RotationAxis;
	FVector InitialOtherLocation;
	FVector InitialThisLocation;
	FTransform InitialRelativeTransform;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = true), Category = "VRControl")
	bool bIsEnabled;
	
	UPROPERTY()
	UMeshComponent *mesh;
	
	UPROPERTY()
	USceneComponent *root;
	
	TWeakObjectPtr<UPrimitiveComponent> InteractingComponent;

	float BreakDistance;
	FVector InitialDropLocation;

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void ResetControlLocation();
	virtual float GetOutput(int OutputIdx) const override { UE_LOG(LogTemp, Fatal, TEXT("Not implemented")); return 0; }
	virtual void SetInput(int InputIdx, float Value) override { UE_LOG(LogTemp, Fatal, TEXT("Not implemented")); }

	virtual void OnGripTick_Implementation(UVRGripper *VRGripper, float DeltaTime);
	virtual void OnGripRelease_Implementation(UVRGripper *VRGripper);
	virtual void OnGrip_Implementation(UVRGripper *VRGripper);
	virtual bool ShouldAttachToHand_Implementation();

	bool CheckIfGripShouldBreak(UVRGripper *VRGripper);
	void InitBase(FTransform initialTransform, UStaticMesh* mesh, EControlInteractibleAxis interactionAxis, bool isEnabled);
	void InitBase(FTransform initialTransform, UProceduralMeshComponent* mesh, EControlInteractibleAxis interactionAxis, bool isEnabled);
	void SetMeshFromPath(FString path, FVector scale = FVector::OneVector);
	void SetMesh(UProceduralMeshComponent * _mesh, FVector scale = FVector::OneVector);	
	inline static FRotator FVectorToFrotator(FVector vec)
	{
		return FRotator(vec.Y, vec.Z, vec.X);
	}
	inline static FVector FRotatorToFVector(FRotator rot)
	{
		return FVector(rot.Roll, rot.Pitch, rot.Yaw);
	}
	inline static FRotator ClampFRotator(FRotator a, FRotator min, FRotator max)
	{
		float roll = a.Roll, yaw = a.Yaw, pitch = a.Pitch;
		roll = FMath::ClampAngle(a.Roll, min.Roll, max.Roll);
		pitch = FMath::ClampAngle(a.Pitch, min.Pitch, max.Pitch);
		yaw = FMath::ClampAngle(a.Yaw, min.Yaw, max.Yaw);
		return FVectorToFrotator(FVector(roll, pitch, yaw));
	}
	bool IsValidOverlap(UPrimitiveComponent * OverlapComponent);

	UFUNCTION()
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
private:
	void SetInteractionAxis(EControlInteractibleAxis newAxis, FVector &vec);
	void EnableMultiplayer();
};
