// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VRControl.h"
#include "VRControl_Button.generated.h"

/**
 *
 */
UENUM()
enum class EButtonType : uint8
{
	Btn_Press, // state 1 only when is hold pressed
	Btn_Toggle_Return, // changes state from 0 to 1 and from 1 to 0 every other press
	Btn_Toggle_Stay, // state one when pressed, has to unpress for 0 state
	Btn_Toggle_Rotate // button like switch on off
};

UCLASS()
class VRCONTROLS_API AVRControl_Button : public AVRControl
{
	GENERATED_BODY()
public:
	AVRControl_Button();
	/**
	* Init for VRControl_Knob
	*
	* @param initialTransform initial transform
	* @param mesh - mesh for the control
	* @param buttonType - type of the button
	* @param interactionAxis - axis in local frame to interact with button
	* @param depressDistance - how deep we can push button on interactionAxis
	* @param buttonEngageDepth - depth at which button changes state, <= depressDistance
	* @param depressSpeed - speed at which button returns to resting position
	* @param minTimeBetweenEngaging - minimum time in seconds between can interact with button
	* @param isPressed - does button start with state 1 or 0 at the beginning
	* @param isEnabled - is button enabled
	*/
	void Init(UStaticMesh* _mesh, FTransform initialTransform = FTransform::Identity, EButtonType buttonType = EButtonType::Btn_Toggle_Rotate,
		EControlInteractibleAxis interactionAxis = EControlInteractibleAxis::Axis_Y, float depressDistance = 3.0f,
		float buttonEngageDepth = 2.0f, float depressSpeed = 150.0f, float minTimeBetweenEngaging = 0.1f,
		bool isPressed = false, bool isEnabled = true);
	/**
	* Init for VRControl_Knob
	*
	* @param initialTransform initial transform
	* @param mesh - mesh for the control
	* @param buttonType - type of the button
	* @param interactionAxis - axis in local frame to interact with button
	* @param rotationAxis - axis in local frame around which button rotates when buttonType == Btn_Toggle_Rotate
	* @param maxAngle - what is the maximum (minimum = -maximum) angle of rotation
	* @param depressDistance - how deep we can push button on interactionAxis
	* @param buttonEngageDepth - depth at which button changes state, <= depressDistance
	* @param depressSpeed - speed at which button returns to resting position
	* @param minTimeBetweenEngaging - minimum time in seconds between can interact with button
	* @param isPressed - does button start with state 1 or 0 at the beginning
	* @param isEnabled - is button enabled
	*/
	void Init(UStaticMesh* _mesh, FTransform initialTransform = FTransform::Identity,
		EButtonType buttonType = EButtonType::Btn_Toggle_Rotate, EControlInteractibleAxis interactionAxis = EControlInteractibleAxis::Axis_Y,
		EControlInteractibleAxis rotationAxis = EControlInteractibleAxis::Axis_X, float maxAngle = 40.0f, float depressDistance = 3.0f,
		float buttonEngageDepth = 2.0f, float depressSpeed = 150.0f, float minTimeBetweenEngaging = 0.1f,
		bool isPressed = false, bool isEnabled = true);
		
	void Init(FTransform initialTransform, UProceduralMeshComponent* _mesh, EButtonType buttonType,
		EControlInteractibleAxis interactionAxis, float depressDistance,
		float buttonEngageDepth, float depressSpeed, float minTimeBetweenEngaging,
		bool isPressed, bool isEnabled);
		
	~AVRControl_Button() {}

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;
private:

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true), Category = "VRControl|VRButton")
	EButtonType buttonType;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMax = 15.0f, ClampMin = 0.2f), Category = "VRControl|VRButton")
	float DepressDistance;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, UIMax = 15.0f, UIMin = 0.2f), Category = "VRControl|VRButton")
	float ButtonEngageDepth;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, UIMin = 1.0f), Category = "VRControl|VRButton")
	float DepressSpeed;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, UIMin = 0.1f), Category = "VRControl|VRButton")
	float MinTimeBetweenEngaging;
	
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, UIMin = 5.0f, UIMax = 50.0f), Category = "VRControl|VRButton")
	float maxAngle;
	
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true), Category = "VRControl|VRButton")
	bool ButtonState;

	float currentPositionOnAxis;
	float LastToggleTime;
	float currentAngle;
	float initialPositionOnAxis;
	bool bToggledThisTouch;

	FRotator rotToSt;

	void InterpretPositionDifferenceOnAxis(float posDiff);
	void ChangeButtonState(bool newState, float timeOfChange);
	void CheckIfStateShouldChange(float pos);
	void InterpolateToCorrectState(float deltaTime);
	void SetButtonToState(bool state);
	float GetDifferenceOnInteractionAxis();
	float GetMinPositionOnInteractionAxis();
};
