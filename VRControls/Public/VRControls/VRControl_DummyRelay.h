// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VRControls/VRControl.h"
#include "VRControl_DummyRelay.generated.h"

/**
 * 
 */
UCLASS()
class VRCONTROLS_API AVRControl_DummyRelay : public AVRControl
{
	GENERATED_BODY()

public:
	AVRControl_DummyRelay();

	virtual float GetOutput(int OutputIdx) const override;

	virtual void SetInput(int InputIdx, float Value) override;
	
protected:

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

private:
	float value;

};
