// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VRControl.h"
#include <Runtime/Engine/Classes/Engine/TextRenderActor.h>
#include "VRControl_Indicator.generated.h"

UCLASS()
class VRCONTROLS_API AVRControl_Indicator : public AVRControl
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVRControl_Indicator();
	~AVRControl_Indicator() {}
	/**
	* Init for VRControl_Indicator
	*
	* @param mesh - mesh for the control
	* @param initialTransform initial transform
	* @param interactionAxis - axis in local frame around which lever rotates
	* @param minAngle - min angle in local frame
	* @param maxAngle - max angle in local frame
	* @param minValue - min angle in local frame
	* @param maxValue - max angle in local frame
	* @param rotationSpeed - speed at which indicator rotates
	* @param isEnabled - is lever enabled to interact
	*/
	void Init(UStaticMesh *_mesh, FTransform initialTransform = FTransform::Identity, EControlInteractibleAxis interactionAxis = EControlInteractibleAxis::Axis_X,
		float minAngle = -45.0f, float maxAngle = 45.0f, float minValue = 8, float maxValue = 16, float rotationSpeed = 50.0f, bool isEnabled = true);

	void SetIndicatorValue(float Value);

	void G_Pressed();
	FText GetFloatAsTextWithPrecision(float TheFloat, int32 Precision, bool IncludeLeadingZero);
	virtual void SetupInput();

protected:

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	virtual void SetInput(int InputIdx, float Value) override;
	
private:
	FQuat rotation, dstRotation;
	float currentAngle; 

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMax = 180.0f), Category = "VRControl|VRIndicator")
	float maxAngle;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMin = -180.0f), Category = "VRControl|VRIndicator")
	float minAngle;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMax = 180.0f), Category = "VRControl|VRIndicator")
	float maxValue;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMin = -180.0f), Category = "VRControl|VRIndicator")
	float minValue;
	
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMax = 150.0f, ClampMin = 0.0f, EditCondition = IsReturningToZero), Category = "VRControl|VRIndicator")
	float RotationSpeed;

	UPROPERTY(EditAnywhere)
	ATextRenderActor *Text;
};
