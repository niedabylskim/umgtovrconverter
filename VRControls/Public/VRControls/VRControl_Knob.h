// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VRControl.h"
#include "VRControl_Knob.generated.h"

/**
 *
 */
UCLASS()
class VRCONTROLS_API AVRControl_Knob : public AVRControl
{
	GENERATED_BODY()
public:
	AVRControl_Knob();
	/**
	* Init for VRControl_Knob
	*
	* @param initialTransform initial transform
	* @param mesh - mesh for the control
	* @param interactionAxis - axis in local frame around which knob rotates
	* @param rotationAxis - axis in local frame around which hand has to rotate to interact with the knob
	* @param minAngle - min angle in local frame
	* @param maxAngle - max angle in local frame
	* @param currentAngle - angle between min angle and max angle at which lever start game
	* @param breakDistance - distance at which grab is released
	* @param rotationScaler - how to scale rotation, negative values possible
	* @param snapAngles - should angles be snapped;
	* @param snapAngleIncrement - discrete state increment
	* @param snapAngleThreshold - minimum delta angle to change state to other discrete state
	* @param isEnabled - is lever enabled to interact
	*/
	void Init(UStaticMesh* _mesh, FTransform initialTransform = FTransform::Identity , EControlInteractibleAxis interactionAxis = EControlInteractibleAxis::Axis_Z,
		EControlInteractibleAxis rotationAxis = EControlInteractibleAxis::Axis_X, float minAngle = 50.0f, float maxAngle = 360.0f, float currentAngle = 100.0f,
		float breakDistance = 50.0f, float rotationScaler = 1.0f, bool snapAngles = false,
		float snapAngleIncrement = 5.0f, float snapAngleThreshold = 2.0f, bool isEnabled = true);
	~AVRControl_Knob() {}
protected:

	virtual void BeginPlay() override;
	virtual void ResetControlLocation() override;
	virtual void OnGripTick_Implementation(UVRGripper *VRGripper, float DeltaTime) override;
	virtual void OnGripRelease_Implementation(UVRGripper *VRGripper) override;
	virtual void OnGrip_Implementation(UVRGripper *VRGripper) override;
private:

	FRotator LastRotation;
	float CurrentAngle;
	float lastAngle;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMax = 360.0f, ClampMin = 0.0f), Category = "VRControl|VRKnob")
	float MaxAngle;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMax = 360.0f, ClampMin = 0.0f), Category = "VRControl|VRKnob")
	float MinAngle;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true), Category = "VRControl|VRKnob")
	bool IsAngleSnapped;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMax = 90.0f, ClampMin = 0.0f, EditCondition = IsAngleSnapped), Category = "VRControl|VRKnob")
	float SnapAngleIncrement;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMax = 10.0f, ClampMin = 0.0f, EditCondition = IsAngleSnapped), Category = "VRControl|VRKnob")
	float SnapAngleThreshold;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMax = 5.0f, ClampMin = -5.0f), Category = "VRControl|VRKnob")
	float RotationScaler;

	void AddAngle(float deltaAngle);
	void ClampAngleToBoundaries(float deltaAngle);
	void UpdateMeshRotaion(float angle);
};
