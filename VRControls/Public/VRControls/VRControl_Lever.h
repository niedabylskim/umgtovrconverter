// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VRControl.h"
#include "VRControl_Lever.generated.h"

/**
 *
 */
UCLASS()
class VRCONTROLS_API AVRControl_Lever : public AVRControl
{
	GENERATED_BODY()

public:
	AVRControl_Lever();
	~AVRControl_Lever() {}
	/**
	* Init for VRControl_Lever
	*
	* @param mesh - mesh for the control
	* @param initialTransform initial transform
	* @param interactionAxis - axis in local frame around which lever rotates
	* @param minAngle - min angle in local frame
	* @param maxAngle - max angle in local frame
	* @param startingAngle - angle between min angle and max angle at which lever start game
	* @param hasDiscreteStates - determines whether lever states are discretized or continuous
	* @param snapAngle - angle step for discretized lever
	* @param isReturningToZero - determines whether lever after grab release returns to starting position (startingAngle)
	* @param returningSpeed - speed at which lever returns
	* @param breakGripDistance - distance at which grab is released
	* @param isEnabled - is lever enabled to interact
	*/
	void Init(UStaticMesh *_mesh, FTransform initialTransform = FTransform::Identity, EControlInteractibleAxis interactionAxis = EControlInteractibleAxis::Axis_X,
		float minAngle = -45.0f, float maxAngle = 45.0f, float startingAngle = 0.0f, bool hasDiscreteStates = true, float snapAngle = 5.0f,
		bool isReturningToZero = false, float returningSpeed = 150.0f, float breakGripDistance = 50.0f, bool isEnabled = true);

protected:

	virtual void BeginPlay() override;
	virtual void ResetControlLocation() override;
	virtual void Tick(float DeltaTime) override;
	virtual void OnGripTick_Implementation(UVRGripper *VRGripper, float DeltaTime) override;
	virtual void OnGripRelease_Implementation(UVRGripper *VRGripper) override;
	virtual void OnGrip_Implementation(UVRGripper *VRGripper) override;

private:
	FVector InitialInteractorLocation;
	FVector InitialInteractorDropLocation;
	FRotator snapAngles;
	FQuat startQuaternion, onreleasegrapQuaternion, quatLev;
	FVector rotationAtGrabVector;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMax = 180.0f), Category = "VRControl|VRLever")
	float maxAngle;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMin = -180.0f), Category = "VRControl|VRLever")
	float minAngle;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true), Category = "VRControl|VRLever")
	bool HasDiscreteStates;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true), Category = "VRControl|VRLever")
	bool IsReturningToZero;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMax = 150.0f, ClampMin = 0.0f, EditCondition = IsReturningToZero), Category = "VRControl|VRLever")
	float ReturningSpeed;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMax = 40.0f, ClampMin = 0.0f, EditCondition = HasDiscreteStates), Category = "VRControl|VRLever")
	float snapAngle;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMin = -180.0f, ClampMax = 180.0f), Category = "VRControl|VRLever")
	float startingAngle;

	bool IsLerpingToZero;

	void SetStartingAngle(float angle);
	FQuat ClampAngle(FQuat q);
};
