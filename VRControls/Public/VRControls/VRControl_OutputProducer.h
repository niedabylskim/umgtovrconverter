// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VRControls/VRControl.h"
#include "VRControl_OutputProducer.generated.h"

/**
 * 
 */
UCLASS()
class VRCONTROLS_API AVRControl_OutputProducer : public AVRControl
{
	GENERATED_BODY()
public:
	AVRControl_OutputProducer();
	void Init(float outputValue);

	virtual float GetOutput(int OutputIdx) const override;

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;	
	
private:
	float OutputValue;
};
