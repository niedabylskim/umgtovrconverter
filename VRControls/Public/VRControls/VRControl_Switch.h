// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VRControl.h"
#include "VRControl_Switch.generated.h"

/**
 *
 */
UCLASS()
class VRCONTROLS_API AVRControl_Switch : public AVRControl
{
	GENERATED_BODY()

public:
	AVRControl_Switch();
	/**
	* Init for VRControl_Switch
	*
	* @param initialTransform initial transform
	* @param mesh - mesh for the control
	* @param interactionAxis - axis in local frame to take controller's velocity from
	* @param rotationAxis - axis in local frame to rotate mesh
	* @param minAngle - angle for 0 state
	* @param maxAngle - angle for numOfStates angle
	* @param numOfStates - number of states for the control
	* @param currentState - state at which the control begins
	* @param minSpeedToInteract - minimum speed at which controller should hit to change state
	* @param minTimeBetweenCollision - minimum time in seconds between change of state
	* @param isEnabled - is control enabled
	*/
	void Init(UStaticMesh* mesh, FTransform initialTransform = FTransform::Identity, EControlInteractibleAxis interactionAxis = EControlInteractibleAxis::Axis_X,
		EControlInteractibleAxis rotationAxis = EControlInteractibleAxis::Axis_X, float minAngle = -30.0f, float maxAngle = 30.0f, float numOfStates = 3,
		float currentState = 1, float minSpeedToInteract = 30.0f, float minTimeBetweenCollision = 0.2f, bool isEnabled = true);
	~AVRControl_Switch() {}
protected:
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
private:

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, UIMin = 0, UIMax = 10, ClampMin = 0, ClampMax = 10), Category = "VRControl|VRSwitchButton")
		int currentState;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, UIMin = 1, UIMax = 10, ClampMin = 1, ClampMax = 10), Category = "VRControl|VRSwitchButton")
		int numberOfStates;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, UIMin = -90.0f, UIMax = 0.0f, ClampMin = -90.0f, ClampMax = 0.0f), Category = "VRControl|VRSwitchButton")
		float minAngle;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, UIMin = 0.0f, UIMax = 90.0f, ClampMin = 0.0f, ClampMax = 90.0f), Category = "VRControl|VRSwitchButton")
		float maxAngle;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, UIMin = 0.1f, UIMax = 2.0f, ClampMin = 0.1f, ClampMax = 2.0f), Category = "VRControl|VRSwitchButton")
		float minTimeBetweenCollision;	
	
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, UIMin = 0.0f, ClampMin = 0.0f), Category = "VRControl|VRSwitchButton")
		float speedThreshold;

	float angleStep;
	float lastSwitchTime;

	void ChangeSwitchState(int stateNum);
	void InterpretVelocity(float time);


};
