// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class VRControls : ModuleRules
{
    public VRControls(ReadOnlyTargetRules Target) : base(Target)
    {
        MinFilesUsingPrecompiledHeaderOverride = 1;
        bFasterWithoutUnity = true;

        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        bEnforceIWYU = true;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "VRUtilities", "ProceduralMeshComponent" });

        PublicIncludePaths.AddRange(new string[] { "VRControls/Public" });

        PrivateIncludePaths.AddRange(new string[] { "VRControls/Private" });
    }
}