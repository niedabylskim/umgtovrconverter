// Fill out your copyright notice in the Description page of Project Settings.

#include "EventManager.h"
#include <Button.h>

// Sets default values for this component's properties
UEventManager::UEventManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	// ...
}


void UEventManager::Init(TMap<UObject*, UWidget*> &_objectConnector)
{
	objectConnector = _objectConnector;
}

// Called when the game starts
void UEventManager::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UEventManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// ...
}

void UEventManager::OnButtonClick(UObject *caller)
{
	auto button = Cast<UButton>(objectConnector[caller]);
	if (button) button->OnClicked.Broadcast();
}

void UEventManager::OnButtonTouch(UObject * caller)
{
	auto button = Cast<UButton>(objectConnector[caller]);
	if (button) button->OnPressed.Broadcast();
}

void UEventManager::OnButtonUntouch(UObject * caller)
{
	auto button = Cast<UButton>(objectConnector[caller]);
	if (button) button->OnReleased.Broadcast();
}

