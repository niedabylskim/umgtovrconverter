// Fill out your copyright notice in the Description page of Project Settings.

#include "GeometryCreator.h"
#include <ProceduralMeshComponent.h>
#include <Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h>
// Sets default values for this component's properties
UGeometryCreator::UGeometryCreator()
{
	PrimaryComponentTick.bCanEverTick = false;
}


void UGeometryCreator::Init(float width, float height, float angleX, float angleY)
{
	Width = width;
	Height = height;
	AngleX = angleX;
	AngleY = angleY;
}

UProceduralMeshComponent * UGeometryCreator::CreateGeometry(FGeometryParams params)
{
	TArray<FVector> vertices;
	TArray<FVector> normals;
	TArray<int32> triangles;
	TArray<FLinearColor> colors;
	TArray<FVector2D> texCoords;
	TArray<FProcMeshTangent> tangents;
	FVector2D uvS, uvE;
	CalculateUVOfObject(params.localSpaceSize, params.renderSpaceSize, uvS, uvE);
	float rangeU = uvE.X - uvS.X;
	float rangeV = uvE.Y - uvS.Y;
	int xTriangles = params.numOfXTriangles;
	int yTriangles = params.numOfYTriangles;
	FProcMeshVertex midVertex = CalculatePointOnPatch(uvS.X + 0.5f * rangeU, uvS.Y + 0.5f * rangeV);
	FTransform tr = FTransform(FQuat::FindBetweenVectors(FVector(1, 0, 0), midVertex.Normal), midVertex.Position, FVector::OneVector);
	for (int i = 0; i < yTriangles; i++)
	{
		for (int j = 0; j < xTriangles; j++)
		{
			float uC = (float)i / (yTriangles - 1);
			float vC = (float)j / (xTriangles - 1);
			float u = uvS.X + uC * rangeU;
			float v = uvS.Y + vC * rangeV;
			FProcMeshVertex vertex = CalculatePointOnPatch(u, v);
			vertex.Position = tr.InverseTransformPosition(vertex.Position);

			vertices.Add(vertex.Position);
			normals.Add(vertex.Normal);
			tangents.Add(vertex.Tangent);
			texCoords.Add(vertex.UV0);
			colors.Add(vertex.Color);

			if (i < yTriangles - 1 && j <xTriangles - 1)
			{
				triangles.Add(i * xTriangles + j + 1);
				triangles.Add(i * xTriangles + j);
				triangles.Add((i + 1) * xTriangles + j);
				triangles.Add((i + 1) * xTriangles + j + 1);
				triangles.Add(i * xTriangles + j + 1);
				triangles.Add((i + 1) * xTriangles + j);
			}
		}
	}

	UProceduralMeshComponent* m = NewObject<UProceduralMeshComponent>(params.parent, UProceduralMeshComponent::StaticClass(), FName(*params.name));
	m->CreateMeshSection_LinearColor(0, vertices, triangles, normals, texCoords, colors, tangents, params.generateCollisions);
	m->bUseAsyncCooking = true;
	m->CastShadow = false;
	m->ContainsPhysicsTriMeshData(true);
	m->SetMaterial(0, params.material);
	m->bUseComplexAsSimpleCollision = false;
	m->bGenerateOverlapEvents = params.generateCollisions;
	m->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	m->TranslucencySortPriority = 1;// offsetFromSurface != 0;
	m->AddCollisionConvexMesh(vertices);
	if (params.objectDepth != 0.0f)
	{
		TArray<FVector> vEdges, nEdges;
		TArray<int32> tEdges;
		TArray<FLinearColor> cEdges;
		TArray<FVector2D> uvEdges;
		TArray<FProcMeshTangent> tanEdges;
		if (params.generateOnlySides) {
			FVector dNb = params.objectDepth*tr.InverseTransformVector(midVertex.Normal);
			CreateEdgeOfDepth(dNb, xTriangles, yTriangles, true, vertices, normals, vEdges, nEdges, tEdges, cEdges, uvEdges, tanEdges);
			CreateEdgeOfDepth(dNb, xTriangles, yTriangles, false, vertices, normals, vEdges, nEdges, tEdges, cEdges, uvEdges, tanEdges);
			m->CreateMeshSection_LinearColor(1, vEdges, tEdges, nEdges, uvEdges, cEdges, tanEdges, false);
		}
		else {
			for (int i = 0; i < vertices.Num(); i++) {
				FVector norm = params.objectDepth*tr.InverseTransformVector(normals[i]);
				vEdges.Add(vertices[i] - norm);
				nEdges.Add(-norm);
			}
			for (int i = 0; i < triangles.Num(); i += 3) {
				tEdges.Add(triangles[i]);
				tEdges.Add(triangles[i + 2]);
				tEdges.Add(triangles[i + 1]);
			}
			FVector dNb = params.objectDepth*tr.InverseTransformVector(midVertex.Normal);
			CreateEdgeOfDepth(dNb, xTriangles, yTriangles, true, vertices, normals, vEdges, nEdges, tEdges, cEdges, uvEdges, tanEdges);
			CreateEdgeOfDepth(dNb, xTriangles, yTriangles, false, vertices, normals, vEdges, nEdges, tEdges, cEdges, uvEdges, tanEdges);
			m->CreateMeshSection_LinearColor(1, vEdges, tEdges, nEdges, uvEdges, cEdges, tanEdges, false);
		}
	}
	if (params.offsetFromSurface != 0.0f)
		tr.SetLocation(tr.GetLocation() + params.offsetFromSurface*midVertex.Normal);
	m->RegisterComponent();
	m->SetWorldTransform(tr);
	return m;
}

// Called when the game starts
void UGeometryCreator::BeginPlay()
{
	Super::BeginPlay();
}

FProcMeshVertex UGeometryCreator::CalculatePointOnPatch(float u, float v)
{
	FProcMeshVertex vertex;
	vertex.UV0 = FVector2D(v, u);
	bool isAngleXAlmostZero = FMath::IsNearlyZero(AngleX, 0.1f);
	bool isAngleYAlmostZero = FMath::IsNearlyZero(AngleY, 0.1f);
	float aXrad = AngleX / 180.0f;
	float aYrad = AngleY / 180.0f;
	float uMaxAngle = isAngleXAlmostZero ? 1.0f : aXrad * PI;
	float vMaxAngle = isAngleYAlmostZero ? 1.0f : aYrad * PI;
	float R = isAngleYAlmostZero ? Width : (Width) / (aYrad * PI);
	float r = isAngleXAlmostZero ? Height : (Height) / (aXrad* PI);

	u = u * uMaxAngle - uMaxAngle / 2.0f;
	v = v * vMaxAngle - vMaxAngle / 2.0f;

	float cosu = FMath::Cos(u);
	float sinu = FMath::Sin(u);
	float cosv = FMath::Cos(v);
	float sinv = FMath::Sin(v);
	float x, y, z;
	FVector du, dv;

	if (isAngleXAlmostZero && isAngleYAlmostZero)
	{
		x = 0.0f;
		z = -R * (u);
		y = -r * (v);
		du = FVector(0, -R, 0);
		dv = FVector(0, 0, r);

	}
	else if (isAngleXAlmostZero)
	{
		x = -(R * cosv - R);
		y = -(R * sinv);
		z = -r * (u);
		du = FVector(0, 0, -r);
		dv = FVector(R*sinv, -R*cosv, 0);
	}
	else if (isAngleYAlmostZero)
	{
		x = -(r * cosu - r);
		y = -R * (v);
		z = -(r * sinu);
		du = FVector(r*sinu, 0, -r*cosu);
		dv = FVector(0, -R, 0);
	}
	else
	{
		R = R - r;
		x = -((R + r * cosu) * cosv - (r + R));
		y = -(R + r * cosu) * sinv;
		z = -r * sinu;
		du = FVector(r*sinu*cosv, r*sinu*sinv, -r*cosu);
		dv = FVector(sinv*(r*cosu + R), cosv*(-r*cosu - R), 0);
	}
	vertex.Position = FVector(x, y, z);
	vertex.Normal = -FVector::CrossProduct(du, dv);
	vertex.Normal.Normalize();
	auto tangent = FVector(du);
	tangent.Normalize();
	vertex.Tangent = FProcMeshTangent(tangent.X, tangent.Y, tangent.Z);
	vertex.Color = FColor(0, 0, 0, 1);
	return vertex;
}

void UGeometryCreator::CalculateUVOfObject(FVector2D localSize, FVector2D renderSize, FVector2D &uvStart, FVector2D &uvEnd)
{
	auto a = localSize;
	auto b = renderSize;
	uvStart.Y = b.X / Width;
	uvStart.X = b.Y / Height;
	uvEnd.Y = (a.X + b.X) / Width;
	uvEnd.X = (a.Y + b.Y) / Height;
}

void UGeometryCreator::CreateEdgeOfDepth(
	FVector depth,
	int xTriangles,
	int yTriangles,
	bool isHorizontal,
	const TArray<FVector> &originalVertices,
	const TArray<FVector> &originalNormals,
	TArray<FVector> &vEdges,
	TArray<FVector> &nEdges,
	TArray<int32> &tEdges,
	TArray<FLinearColor> &cEdges,
	TArray<FVector2D> &uvEdges,
	TArray<FProcMeshTangent> &tanEdges)
{
	int range = isHorizontal ? yTriangles - 1 : xTriangles - 1;
	for (int i = 0; i < range; i++)
	{
		int e1a, e1b, e2a, e2b;
		if (isHorizontal) {
			e1a = i * xTriangles;
			e1b = (i + 1) * xTriangles;
			e2a = i * xTriangles + xTriangles - 1;
			e2b = (i + 1) * xTriangles + xTriangles - 1;
		}
		else
		{
			e2a = i;
			e2b = i + 1;
			e1a = (yTriangles - 1)*xTriangles + i;
			e1b = (yTriangles - 1)*xTriangles + i + 1;
		}
		auto v1a = originalVertices[e1a];
		auto v2a = originalVertices[e2a];
		auto n1a = originalNormals[e1a];
		auto n2a = originalNormals[e2a];
		auto v1b = originalVertices[e1b];
		auto v2b = originalVertices[e2b];
		auto n1b = originalNormals[e1b];
		auto n2b = originalNormals[e2b];

		vEdges.Add(v1a);
		vEdges.Add(v1b);
		vEdges.Add(v1a - depth);
		vEdges.Add(v1b - depth);
		vEdges.Add(v2a);
		vEdges.Add(v2b);
		vEdges.Add(v2a - depth);
		vEdges.Add(v2b - depth);

		nEdges.Add(n1a);
		nEdges.Add(n1b);
		nEdges.Add(FVector(1, 0, 0));
		nEdges.Add(FVector(1, 0, 0));
		nEdges.Add(n2a);
		nEdges.Add(n2b);
		nEdges.Add(FVector(1, 0, 0));
		nEdges.Add(FVector(1, 0, 0));

		cEdges.Add(FLinearColor(1, 0, 0, 1));
		cEdges.Add(FLinearColor(1, 0, 0, 1));
		cEdges.Add(FLinearColor(1, 0, 0, 1));
		cEdges.Add(FLinearColor(1, 0, 0, 1));
		cEdges.Add(FLinearColor(1, 0, 0, 1));
		cEdges.Add(FLinearColor(1, 0, 0, 1));
		cEdges.Add(FLinearColor(1, 0, 0, 1));
		cEdges.Add(FLinearColor(1, 0, 0, 1));

		int t1a = vEdges.Num() - 4;
		int t2a = vEdges.Num() - 3;
		int t3a = vEdges.Num() - 2;
		int t4a = vEdges.Num() - 1;
		int t1b = vEdges.Num() - 5;
		int t2b = vEdges.Num() - 6;
		int t3b = vEdges.Num() - 7;
		int t4b = vEdges.Num() - 8;

		tEdges.Add(t2b);
		tEdges.Add(t1b);
		tEdges.Add(t3b);
		tEdges.Add(t4b);
		tEdges.Add(t2b);
		tEdges.Add(t3b);
		tEdges.Add(t1a);
		tEdges.Add(t2a);
		tEdges.Add(t3a);
		tEdges.Add(t2a);
		tEdges.Add(t4a);
		tEdges.Add(t3a);
	}
}
