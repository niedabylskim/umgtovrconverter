// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuBackend.h"


// Sets default values
UMenuBackend::UMenuBackend()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void UMenuBackend::Init(TMap<UWidget*, UObject*> &objConnector)
{
	menuObjects = objConnector;
}

void UMenuBackend::BeginPlay()
{
	Super::BeginPlay();
}

void UMenuBackend::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

UObject* UMenuBackend::GetCorresponding3DObj(UWidget *obj2D_UMG)
{
	return menuObjects[obj2D_UMG];
}

// Called when the game starts or when spawned
//void UMenuBackend::BeginPlay()
//{
//	Super::BeginPlay();
//	UE_LOG(LogTemp, Error, TEXT("UMenuBackend::BeginPlay()"));
//}