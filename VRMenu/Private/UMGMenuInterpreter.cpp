// Fill out your copyright notice in the Description page of Project Settings.

#include "UMGMenuInterpreter.h"
#include "DrawDebugHelpers.h"
#include <CanvasPanelSlot.h>
#include <WidgetTree.h>
#include <Geometry.h>
#include <Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h>
#include <Runtime/CoreUObject/Public/UObject/UObjectGlobals.h>
#include <Runtime/Engine/Classes/Engine/StaticMesh.h>
#include <Developer/RawMesh/Public/RawMesh.h>
#include <DrawDebugHelpers.h>
#include "VRControl_Button.h"
#include <Runtime/UMG/Public/Components/Button.h>
#include <Components/Image.h>
//#include <GeometryCreator.h>

// Sets default values

AUMGMenuInterpreter::AUMGMenuInterpreter()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
	sceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneComponent"));
	eventManager = CreateDefaultSubobject<UEventManager>(TEXT("EventManager"));
	geometryCreator = CreateDefaultSubobject<UGeometryCreator>(TEXT("GeometryCreator"));
	widgetRenderer = MakeShareable(new FWidgetRenderer(true));
	RootComponent = sceneComponent;
	numOfXTriangles = 10;
	numOfYTriangles = 10;
	AngleX = 60.1f;
	AngleY = 60.1f;
	Width = 1280;
	Height = 720;
}

void AUMGMenuInterpreter::InitializeMenu()
{
	// Don't do any work if Slate is not initialized
	if (FSlateApplication::IsInitialized())
	{
		if (WidgetClass && menu == nullptr && GetWorld())
		{
			menu = CreateWidget<UUserWidget>(GetWorld(), WidgetClass);
		}

#if WITH_EDITOR
		if (menu && !GetWorld()->IsGameWorld())
		{
			//if (!GEnableVREditorHacks)
			{
				// Prevent native ticking of editor component previews
				menu->SetDesignerFlags(EWidgetDesignFlags::Designing);

			}
		}
#endif
	}
}

void AUMGMenuInterpreter::InitializeMenuSize()
{
	SlateWindow = SNew(SVirtualWindow).Size(DrawSize);
	if (SlateWindow.IsValid())
	{
		if (FSlateApplication::IsInitialized())
			FSlateApplication::Get().RegisterVirtualWindow(SlateWindow.ToSharedRef());
		TSharedPtr<SWidget> widget = menu->TakeWidget();
		SlateWindow->SetContent(widget.ToSharedRef());
		SlateWindow->SlatePrepass(1.0f);
		FVector2D DesiredSize = SlateWindow->GetDesiredSizeDesktopPixels();
		DesiredSize.X = FMath::RoundToInt(DesiredSize.X);
		DesiredSize.Y = FMath::RoundToInt(DesiredSize.Y);
		DrawSize = DesiredSize.IntPoint();
		Width = DrawSize.X;
		Height = DrawSize.Y;
	}
}

void AUMGMenuInterpreter::InitializeMaterialAndTexture()
{
	texture_fullMenu = TextureFromWidget(menu, DrawSize);
	materialInstance = UMaterialInstanceDynamic::Create(material, this);
	materialInstance->SetTextureParameterValue(FName("texture"), texture_fullMenu);
}

void AUMGMenuInterpreter::InitializeMenuBackendObject()
{
	auto ev = menu->GetClass()->FindPropertyByName("menuEvents");
	auto obj = Cast<UObjectProperty>(ev);
	if (obj != nullptr)	obj->SetPropertyValue_InContainer(menu, backend);
	else UE_LOG(LogTemp, Error, TEXT("NO BACKEND FILE SETUP"));
}

// Called when the game starts or when spawned
void AUMGMenuInterpreter::BeginPlay()
{
	Super::BeginPlay();
	if (*MenuClass != nullptr) {
		backend = NewObject<UMenuBackend>(this, *MenuClass, TEXT("MenuBackend"));
		backend->RegisterComponent();
	}
	InitializeMenu();
	if (!menu) return;
	InitializeMenuSize();
	InitializeMaterialAndTexture();
	InitializeMenuBackendObject();
	geometryCreator->Init(Width, Height, AngleX, AngleY);
	Create3DMenu();
}

void AUMGMenuInterpreter::Tick(float DeltaTime)
{
	//TODO redraw menu if neccessary
}

void AUMGMenuInterpreter::Create3DMenu()
{
	TArray<UWidget*> children;
	UWorld* World = GetWorld();
	menu->WidgetTree->GetChildWidgets(menu->GetRootWidget(), children);
	auto mesh = geometryCreator->CreateGeometry(
		FGeometryParams(this, "Root", FVector2D(Width, Height), FVector2D(0, 0),
			materialInstance, 0, 0, false, false, 5, 5));
	mesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	for (int i = 0; i < children.Num(); i++)
	{
		AVRControl *control = nullptr;
		auto className = children[i]->GetClass();
		auto geometry = children[i]->GetCachedGeometry();
		if (className == UButton::StaticClass())
		{
			control = CreateVRButtonFromUmgButton(Cast<UButton>(children[i]));
		}
		else if (className == UImage::StaticClass())
		{
			if (children[i]->GetName() == "Background_Main")
			{
				auto texture = TextureFromWidget(children[i], DrawSize);
				auto mat = UMaterialInstanceDynamic::Create(material, this);
				mat->SetTextureParameterValue(FName("texture"), texture);
				UE_LOG(LogTemp, Warning, TEXT("set"));
				mesh->SetMaterial(0, mat);
			}
		}
		umgTo3DConnector.Add(children[i], control);
	}
	eventManager->Init(objectConnector);
	backend->Init(umgTo3DConnector);
}

AVRControl* AUMGMenuInterpreter::CreateVRButtonFromUmgButton(UButton *button)
{
	auto geometry = button->GetCachedGeometry();
	bool isClickedEventSetup = button->OnClicked.IsBound();
	AVRControl_Button *control = GetWorld()->SpawnActorDeferred<AVRControl_Button>(AVRControl_Button::StaticClass(), FTransform::Identity, this);
	auto m = geometryCreator->CreateGeometry(
		FGeometryParams(
			control, button->GetName(), geometry.GetLocalSize(),
			geometry.GetAbsolutePosition(),
			materialInstance, 20.0f, 7.0f, true, false, 10, 10
		));
	control->Init(FTransform::Identity, m, EButtonType::Btn_Toggle_Return,
		EControlInteractibleAxis::Axis_X, 15, 13, 50, 0.2, false, true);
	control->FinishSpawning(FTransform::Identity);
	control->GetRootComponent()->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	objectConnector.Add(control, button);

	if (isClickedEventSetup)
		control->OnStateChanged.AddDynamic(eventManager, &UEventManager::OnButtonClick);
	return control;
}

UTexture2D* AUMGMenuInterpreter::TextureFromWidget(UWidget *const Widget, const FVector2D &DrawSize)
{
	if (FSlateApplication::IsInitialized()
		&& Widget != NULL && Widget->IsValidLowLevel()
		&& DrawSize.X >= 1 && DrawSize.Y >= 1)
	{
		TSharedPtr<SWidget> SlateWidget(Widget->TakeWidget());
		if (!SlateWidget.IsValid()) return NULL;

		if (!widgetRenderer.IsValid()) return NULL;

		UTextureRenderTarget2D *TextureRenderTarget = widgetRenderer->DrawWidget(SlateWidget.ToSharedRef(), DrawSize);

		// Creates Texture2D to store RenderTexture content
		UTexture2D *Texture = UTexture2D::CreateTransient(DrawSize.X, DrawSize.Y, PF_B8G8R8A8);
#if WITH_EDITORONLY_DATA
		Texture->MipGenSettings = TMGS_NoMipmaps;
#endif

		// Lock and copies the data between the textures
		TArray<FColor> SurfData;
		FRenderTarget *RenderTarget = TextureRenderTarget->GameThread_GetRenderTargetResource();
		RenderTarget->ReadPixels(SurfData);

		void* TextureData = Texture->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
		const int32 TextureDataSize = SurfData.Num() * 4;
		FMemory::Memcpy(TextureData, SurfData.GetData(), TextureDataSize);
		Texture->PlatformData->Mips[0].BulkData.Unlock();
		Texture->UpdateResource();

		// Free resources
		SurfData.Empty();
		TextureRenderTarget->ConditionalBeginDestroy();
		SlateWidget.Reset();

		return Texture;
	}
	return NULL;
}