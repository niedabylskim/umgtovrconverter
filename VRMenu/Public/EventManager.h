// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include <UserWidget.h>
#include "EventManager.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VRMENU_API UEventManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEventManager();
	void Init(TMap<UObject*, UWidget*> &objectConnector);
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
private:
	UPROPERTY()
	TMap<UObject*, UWidget*> objectConnector;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	void OnButtonClick(UObject *caller);
	UFUNCTION()
	void OnButtonTouch(UObject *caller);
	UFUNCTION()
	void OnButtonUntouch(UObject *caller);
	

};
