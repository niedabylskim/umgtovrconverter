// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GeometryCreator.generated.h"

class UProceduralMeshComponent;
struct FProcMeshVertex;
struct FLinearColor;
struct FProcMeshTangent;

USTRUCT()
struct FGeometryParams
{
	GENERATED_BODY()
	UObject *parent;
	FString name;
	FVector2D localSpaceSize;
	FVector2D renderSpaceSize;
	UMaterialInstanceDynamic* material;
	float offsetFromSurface;
	float objectDepth;
	bool generateCollisions;
	bool generateOnlySides;
	int numOfXTriangles;
	int numOfYTriangles;
	UTexture2D *texture;
	FGeometryParams(UObject *_parent, FString _name, FVector2D _localSpaceSize,
		FVector2D _renderSpaceSize, UMaterialInstanceDynamic *_material, float _offsetFromSurface,
		float _objectDepth, bool _generateCollisions, bool _generateOnlySides, int _xTriangles, int _yTriangles)
	{
		parent = _parent;
		name = _name;
		localSpaceSize = _localSpaceSize;
		renderSpaceSize = _renderSpaceSize;
		material = _material;
		offsetFromSurface = _offsetFromSurface;
		objectDepth = _objectDepth;
		generateCollisions = _generateCollisions;
		generateOnlySides = _generateOnlySides;
		numOfXTriangles = _xTriangles;
		numOfYTriangles = _yTriangles;
	}//Constructor
	FGeometryParams()
	{
		parent = nullptr;
		name = "";
		localSpaceSize = FVector2D(0,0);
		renderSpaceSize = FVector2D(0, 0);
		material = nullptr;
		offsetFromSurface = 0;
		objectDepth = 0;
		generateCollisions = false;
		numOfXTriangles = 0;
		numOfYTriangles = 0;
		texture = nullptr;
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VRMENU_API UGeometryCreator : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGeometryCreator();
	void Init(float width, float height, float angleX, float angleY);
	UProceduralMeshComponent *CreateGeometry(FGeometryParams params);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:	
	
	FProcMeshVertex CalculatePointOnPatch(float u, float v);
	void CalculateUVOfObject(FVector2D localSize, FVector2D renderSize, FVector2D &uvStart, FVector2D &uvEnd);

	//depth is a depth * normal to surface in the middle point
	void CreateEdgeOfDepth(FVector depth, int xTriangles, int yTriangles, bool isHorizontal, const TArray<FVector>& originalVertices, const TArray<FVector>& originalNormals, TArray<FVector>& vEdges, TArray<FVector>& nEdges, TArray<int32>& tEdges, TArray<FLinearColor>& cEdges, TArray<FVector2D>& uvEdges, TArray<FProcMeshTangent>& tanEdges);

	float Width;
	float Height;
	float AngleX;
	float AngleY;
};
