// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <Components/ActorComponent.h>
#include <UserWidget.h>
#include "MenuBackend.generated.h"
UCLASS(Abstract)
class VRMENU_API UMenuBackend : public UActorComponent
{
public:

	GENERATED_BODY()

	UMenuBackend();

	virtual ~UMenuBackend() {}

	void Init(TMap<UWidget*, UObject*> &objConnector);

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	UObject* GetCorresponding3DObj(UWidget *obj2D_UMG);

private:

	UPROPERTY()
	TMap<UWidget*, UObject*> menuObjects;
	
};
