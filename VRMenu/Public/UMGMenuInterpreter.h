// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <ProceduralMeshComponent.h>
#include <UserWidget.h>
#include <SlateApplication.h>
#include <WidgetRenderer.h>
#include <Runtime/Engine/Classes/Engine/Texture2D.h>
#include <Runtime/Engine/Classes/Engine/TextureRenderTarget2D.h>
#include <UnrealClient.h>
#include <MenuBackend.h>
#include <EventManager.h>
#include <GeometryCreator.h>
#include "UMGMenuInterpreter.generated.h"

class AVRControl;
class UButton;

UCLASS()
class VRMENU_API AUMGMenuInterpreter : public AActor
{
	GENERATED_BODY()
	
public:	
	AUMGMenuInterpreter();
	~AUMGMenuInterpreter() {
		widgetRenderer.Reset();
	}
protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
private:

	UPROPERTY(EditAnywhere)
	USceneComponent *sceneComponent;	
	
	UPROPERTY()
	UGeometryCreator *geometryCreator;

	UPROPERTY()
	TMap<UObject*, UWidget*> objectConnector;

	UPROPERTY()
	TMap<UWidget*,UObject*> umgTo3DConnector;

	TSharedPtr<FWidgetRenderer> widgetRenderer;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMin = 0.0f, ClampMax = 360.0f), Category = "VRPanel")
		float AngleX;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMin = 0.0f, ClampMax = 360.0f), Category = "VRPanel")
		float AngleY;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMin = 0, ClampMax = 2500), Category = "VRPanel")
		int Width;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMin = 0, ClampMax = 2500), Category = "VRPanel")
		int Height;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMin = 1, ClampMax = 50), Category = "VRPanel")
		int numOfXTriangles;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true, ClampMin = 1, ClampMax = 50), Category = "VRPanel")
		int numOfYTriangles;

	UPROPERTY(Transient, DuplicateTransient)
	UUserWidget* menu;

	UTexture2D *texture_fullMenu;

	TSharedPtr<class SVirtualWindow> SlateWindow;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true))
		UMaterialInterface * material;

	UPROPERTY()
		UEventManager* eventManager;

	UPROPERTY()
		UMaterialInstanceDynamic* materialInstance;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true), Category = "VRPanel")
		TSubclassOf<UUserWidget> WidgetClass;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = true), Category = "VRPanel")
		TSubclassOf<UMenuBackend> MenuClass;

	UPROPERTY()
		UMenuBackend *backend;
	FVector2D DrawSize;

	void Create3DMenu();
	AVRControl* CreateVRButtonFromUmgButton(UButton *button);
	void InitializeMenu();
	void InitializeMenuSize();
	void InitializeMaterialAndTexture();
	void InitializeMenuBackendObject();
	UTexture2D * TextureFromWidget(UWidget * const Widget, const FVector2D & DrawSize);
};
