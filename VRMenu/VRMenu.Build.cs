// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class VRMenu : ModuleRules
{
    public VRMenu(ReadOnlyTargetRules Target) : base(Target)
    {
        MinFilesUsingPrecompiledHeaderOverride = 1;
        bFasterWithoutUnity = true;

        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        bEnforceIWYU = true;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "ProceduralMeshComponent", "Slate", "SlateCore", "UMG", "RawMesh", "VRControls", "VRHumanInterfaceDevice" });

        PublicIncludePaths.AddRange(new string[] { "VRMenu/Public" });

        PrivateIncludePaths.AddRange(new string[] { "VRMenu/Private" });
    }
}
